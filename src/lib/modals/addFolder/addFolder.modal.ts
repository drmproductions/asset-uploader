import { Component, Inject, ViewEncapsulation } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormsModule } from '@angular/forms';

export interface DialogData {
  directory: string;
}

@Component({
  selector: 'add-folder-dialog',
  templateUrl: 'addFolder.modal.html',
  styleUrls: ['./addFolder.modal.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddFolderModal {
  constructor(
    public dialogRef: MatDialogRef<AddFolderModal>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onClose(): void {
    this.dialogRef.close(this.data.directory);
  }
}