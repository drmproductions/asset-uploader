import { Component, Inject, ViewEncapsulation } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface DialogData {
  error: any;
  messge: string;
  code: string;
}

@Component({
  selector: 'app-error-dialog',
  templateUrl: 'error.component.html',
  styleUrls: ['./error.component.scss']
})

export class ErrorComponent {
  code: string;
  message: string;

  constructor(
    public dialogRef: MatDialogRef<ErrorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.code = (this.data.error) ? this.getObject(this.data.error, 'code') : this.getObject(this.data, 'code');
    this.message = (this.data.error) ? this.getObject(this.data.error, 'message') : this.getObject(this.data, 'message');
  }

  onClose () {
    this.dialogRef.close();
  }

  getObject(theObject: any, key: string): any {
    let result = null;
    if (theObject instanceof Array) {
      for (let i = 0; i < theObject.length; i++) {
        result = this.getObject(theObject[i], key);
        if (result) {
          break;
        }
      }
    } else {
      for (const prop in theObject) {
        if (true) {
          if (prop === key) {
            return theObject[prop];
          }
          if (theObject[prop] instanceof Object || theObject[prop] instanceof Array) {
            result = this.getObject(theObject[prop], key);
            if (result) {
              break;
            }
          }
        }
      }
    }
    return result;
  }
}
