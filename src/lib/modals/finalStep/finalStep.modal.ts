import { Component, Inject, ViewEncapsulation } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormsModule } from '@angular/forms';

export interface FinalData {
  directory: string;
}

@Component({
  selector: 'final-step-dialog',
  templateUrl: 'finalStep.modal.html',
  styleUrls: ['./finalStep.modal.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FinalStepModal {

  results = {

  };

  constructor(
    public dialogRef: MatDialogRef<FinalStepModal>,
    @Inject(MAT_DIALOG_DATA) public data: FinalData) {
  }

  onClose(): void {
    this.dialogRef.close(this.data.directory);
  }
}