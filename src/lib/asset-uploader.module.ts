import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Angulartics2Module } from 'angulartics2';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SortService } from './medialibrary/sort.service';
import { HttpClientModule} from '@angular/common/http';
import {EditorComponent} from './image-uploader/editor/editor.component';
import {MedialibraryService} from './medialibrary/medialibrary.service';
import {DirectoryService} from './medialibrary/directory.service';
import {EditorService} from './image-uploader/editor/editor.service';
import {ScriptLoaderService} from './image-uploader/editor/scriptLoaderService';
import {
  MatButtonModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatOptionModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ApolloModule} from 'apollo-angular';
import {MedialibraryModule} from './medialibrary/medialibrary.module';
import {ImageUploaderModule} from './image-uploader/image-uploader.module';
import {MedialibraryComponent} from './medialibrary/medialibrary.component';
import {MainmodalModule} from './image-uploader/editor/mainmodal/mainmodal.module';
import {ErrorComponent} from './modals/error/error.component';
import {AddFolderModal} from './modals/addFolder/addFolder.modal';
import {FinalStepModal} from './modals/finalStep/finalStep.modal';
import {AssetUploaderComponent} from './asset-uploader.component';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material';
import {PulldownService} from './image-uploader/pulldowns/pulldown.service';
import {FinalStepPullDown} from './image-uploader/pulldowns/finalstep/finalStep';
import {MainmodalComponent} from './image-uploader/editor/mainmodal/mainmodal.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    Angulartics2Module,
    FontAwesomeModule,
    HttpClientModule,
    MainmodalModule,
    MedialibraryModule,
    ImageUploaderModule,
    ApolloModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatCheckboxModule,
    FormsModule,
    MatInputModule,
  ],
  declarations: [
    MedialibraryComponent,
    EditorComponent,
    ErrorComponent,
    AssetUploaderComponent,
    AddFolderModal,
    FinalStepPullDown
  ],
  providers: [
    EditorService, MedialibraryService, SortService, DirectoryService, ScriptLoaderService, PulldownService
  ],
  entryComponents: [ AddFolderModal, FinalStepModal, FinalStepPullDown, MainmodalComponent ],
  exports: [ MedialibraryComponent, EditorComponent, ErrorComponent, AssetUploaderComponent, AddFolderModal, FinalStepModal ]
})
export class AssetUploaderModule { }
