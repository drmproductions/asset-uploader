import { TestBed, inject } from '@angular/core/testing';

import { AssetUploaderService } from './asset-uploader.service';

describe('AssetUploaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetUploaderService]
    });
  });

  it('should be created', inject([AssetUploaderService], (service: AssetUploaderService) => {
    expect(service).toBeTruthy();
  }));
});
