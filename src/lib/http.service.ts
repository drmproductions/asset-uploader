import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie';
import {MatDialog} from '@angular/material';
import { catchError } from 'rxjs/internal/operators';
import { throwError } from 'rxjs';
import {ErrorComponent} from './modals/error/error.component';

@Injectable()
export class HttpService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + this.cookie.get('token')
    })
  };

  constructor(public http: HttpClient, public cookie: CookieService, public dialog: MatDialog) {}

  call(url: string, type: string, query: object = {}): Observable<Object> {
    if (type.toLowerCase() === 'get') {
      return this.http.get<Object>(url, this.httpOptions)
        .pipe(
          catchError(this.handleError(''))
        );
    } else {
      return this.http.post<Object>(url, query, this.httpOptions)
        .pipe(
          catchError(this.handleError(''))
        );
    }
  }

  sub(route: string, type: string, dataKey: string): Observable<Object> {
    return new Observable(observer => {
      // if (this[dataKey]) {
      //   observer.next(this[dataKey]);
      //   return observer.complete();
      // }
      // this.http
      //   .get(this.editor.authHost + route, this.httpOptions)
      //   .subscribe((results: any) => {
      //     if (results[dataKey]) {
      //       this[dataKey] = results[dataKey];
      //       observer.next(this[dataKey]);
      //     }
      //     observer.complete();
      //   });
    });
  }

  openDialog (data: any) {
    console.log(data);
    const dialogRef = this.dialog.open(ErrorComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('result');
    });
  }

  public handleError(operation: String) {
    return (err: any) => {
      const errMsg = `error in ${operation}()`;

      this.openDialog(err);

      return throwError('');
    };
  }
}
