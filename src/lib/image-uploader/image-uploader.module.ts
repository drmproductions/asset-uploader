import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {CookieModule, CookieService} from 'ngx-cookie';
import {ApolloModule} from 'apollo-angular';
import {
  GestureConfig,
  MatBadgeModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSliderModule,
  MatIconModule, MatDialogModule
} from '@angular/material';
import { InitiatebuttonComponent } from './editor/initiatebutton/initiatebutton.component';
import {MainmodalModule} from './editor/mainmodal/mainmodal.module';
import {ScriptLoaderService} from './editor/scriptLoaderService';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {AssetlibraryModule} from './editor/mainmodal/assetselector/assetlibrary/assetlibrary.module';
import {FinalStepModal} from '../modals/finalStep/finalStep.modal';

@NgModule({
    imports: [
      CommonModule,
      HttpClientModule,
      CookieModule.forChild(),
      ApolloModule,
      MatButtonModule,
      ReactiveFormsModule,
      FormsModule,
      MatIconModule,
      MatFormFieldModule,
      MatIconModule,
      MatBadgeModule,
      MatSliderModule,
      MatInputModule,
      MatDialogModule,

      MainmodalModule,
      AssetlibraryModule
    ],
    declarations: [ InitiatebuttonComponent, FinalStepModal ],
    exports: [ InitiatebuttonComponent, FinalStepModal ],
    providers: [ CookieService, ScriptLoaderService, { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig } ],
  entryComponents: [ FinalStepModal ]
})
export class ImageUploaderModule {
    constructor () {}
}
