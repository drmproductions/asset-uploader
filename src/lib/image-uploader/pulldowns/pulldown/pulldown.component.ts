// @ts-ignore
import {
    Component,
    OnInit,
    Output,
    ViewChild,
    ElementRef,
    OnChanges,
    EventEmitter,
    AfterViewInit, ViewEncapsulation, ComponentRef
} from '@angular/core';
import { TimelineMax } from 'gsap';
import {PulldownService} from '../pulldown.service';

@Component({
    selector: 'lib-pulldown',
    templateUrl: './pulldown.component.html',
    styleUrls: ['./pulldown.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PulldownComponent implements OnInit, OnChanges, AfterViewInit {
    @ViewChild('pulldown') pulldownElement: ElementRef;

    @Output() close = new EventEmitter();
    data: any;

    confirm: boolean;

    Timeline: TimelineMax;

    componentRef: ComponentRef<any>;

    id: number;

    constructor (public pulldown: PulldownService) {
        this.Timeline = new TimelineMax({
            onReverseComplete: () => {
                this.close.emit(this.data);
                this.pulldown.removeComponentFromBody(this.componentRef, this.id);
            }
        });
    }

    ngOnInit() {
        console.log(this.pulldownElement);
        this.Timeline.to(this.pulldownElement.nativeElement, .2, {y: '0%'});
    }

    ngAfterViewInit(): void {
    }

    ngOnChanges() {
    }

    closePulldown() {
        this.Timeline.reverse();
    }
}
