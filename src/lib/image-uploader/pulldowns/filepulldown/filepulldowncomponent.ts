// @ts-ignore
import {
    Component,
    OnInit,
    Input,
    Output,
    OnChanges,
    EventEmitter,
    AfterViewInit,
    ChangeDetectorRef,
    ComponentFactoryResolver, ApplicationRef, Injector
} from '@angular/core';
import {PulldownComponent} from '../pulldown/pulldown.component';
import {AssetFile} from '../../../interfaces/asset.file';
import {PulldownService} from '../pulldown.service';

@Component({
    selector: 'lib-file-pulldown',
    templateUrl: './filepulldown.component.html',
    styleUrls: ['../pulldown/pulldown.component.css']
})
export class FilepulldownComponent extends PulldownComponent implements OnInit, OnChanges, AfterViewInit {

    @Input() file: AssetFile;
    @Input() directories: Array<AssetFile>;

    @Output() rename = new EventEmitter();
    @Output() open = new EventEmitter();
    @Output() move = new EventEmitter();
    @Output() delete = new EventEmitter();

    constructor (public pulldown: PulldownService) {
        super(pulldown);
        this.id = this.pulldown.ids.FILE;
    }

    ngOnInit (): void  {
        super.ngOnInit();
        console.log('pull data', this.data);
        if (this.data.file) {
            this.file = this.data.file;
        }
    }

    ngOnChanges (): void {
    }

    ngAfterViewInit (): void {
        super.ngAfterViewInit();
    }

    onRename (newName: string): void {
        console.log('on rename', newName);
        this.data.file.name = newName;
        this.rename.emit(this.data.file);
    }

    onOpen (): void {
        this.open.emit(this.data.file);
    }

    onMove (newPath: string): void {
        this.data.newPath = newPath;
        this.move.emit(this.data);
    }

    onDelete (): void {
        this.delete.emit(this.data.file);
    }
}
