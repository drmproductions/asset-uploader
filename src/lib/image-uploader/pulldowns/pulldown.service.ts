import {
    Injectable,
    Injector,
    ComponentFactoryResolver,
    EmbeddedViewRef,
    ApplicationRef, ComponentRef, StaticProvider
} from '@angular/core';

@Injectable()
export class PulldownService {

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private appRef: ApplicationRef,
        private injector: Injector
    ) { }

    pulldowns = {};
    ids = {
        'FINAL': 0,
        'FILE': 1,
        'MAIN': 3
    };

    appendComponentToBody(component: any, id: number, data?: any) {
        // 1. Create a component reference from the component
        const componentRef = this.componentFactoryResolver
            .resolveComponentFactory(component)
            .create(this.injector);
        // Attach data to instance //
        if (data) {
            (<any>componentRef.instance).data = data;
        }
        (<any>componentRef.instance).componentRef = componentRef;
        // Add to ref list to subscribe to outputs //
        this.pulldowns[id] = {
            instance: componentRef.instance
        };

        // 2. Attach component to the appRef so that it's inside the ng component tree
        this.appRef.attachView(componentRef.hostView);
        // 3. Get DOM element from component
        const domElem = (componentRef.hostView as EmbeddedViewRef<any>)
            .rootNodes[0] as HTMLElement;
        // 4. Append DOM element to the body
        document.body.appendChild(domElem);
    }

    removeComponentFromBody(componentRef: ComponentRef < any >, id: number ) {
        console.log(componentRef, id);
        delete this.pulldowns[id];
        this.appRef.detachView(componentRef.hostView);
        componentRef.destroy();
    }

    getRef (id: number): ComponentRef<any> {
        if (this.pulldowns[id]) {
            return this.pulldowns[id].instance.componentRef;
        }

        console.log('Bad id or empty pulldowns');

        return null;
    }
}
