import {Component, EventEmitter, Output, ViewEncapsulation} from '@angular/core';
import {PulldownComponent} from '../pulldown/pulldown.component';
import {PulldownService} from "../pulldown.service";

export interface FinalData {
  directory: string;
}

@Component({
  selector: 'lib-final-step-pulldown',
  templateUrl: './finalStep.html',
  styleUrls: ['../pulldown/pulldown.component.css', './finalStep.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FinalStepPullDown extends PulldownComponent {

  @Output() finish = new EventEmitter();

  directory: string;
  filename: string;
  isLibraryAsset: boolean;

  constructor (public pulldown: PulldownService) {
    super(pulldown);
    this.id = this.pulldown.ids.FINAL;
  }

  onFinish () {
    this.closePulldown();
    this.finish.emit({directory: this.directory, isLibraryAsset: this.isLibraryAsset, name: this.filename});
  }
}
