import {Component, OnInit, Input, Output, ViewEncapsulation, ViewChild} from '@angular/core';
import {EditorService} from './editor.service';
import {InitiatebuttonComponent} from './initiatebutton/initiatebutton.component';
import {HttpLink} from 'apollo-angular-link-http';
import {Apollo} from 'apollo-angular';
import {CookieService} from 'ngx-cookie';
import gql from 'graphql-tag';
import {setContext} from 'apollo-link-context';
import {HttpHeaders} from '@angular/common/http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {GraphHosts} from '../../interfaces/graph.hosts';
import {GraphHost} from '../../interfaces/graph.host';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.css'],
    providers:  [ EditorService ],
    encapsulation: ViewEncapsulation.None
})
export class EditorComponent implements OnInit {
  @ViewChild(InitiatebuttonComponent) initButton: InitiatebuttonComponent;

  @Input() imageHost: string;
  @Input() authHost: string;
  @Input() videoHost: string;
  // index in cookie index where auth token is //
  @Input() localAuthKey = 'token';
  @Input() multiple: boolean = null;
  @Input() callback: Function = null;
  @Input() uploadButtonText = 'Upload Text';
  @Input() graphQLResponse = 'auth{status, token}, resources{ url }';
  @Input() asset: File | string = null;
  @Input() unsplashKey: string;
  @Input() fbAppId: string;
  @Input() in_library = false;
  @Input() user: any;
  // Identify where event came from to identify where it goes //
  @Input() assetSource = '';

  constructor(public editor: EditorService, private httpLink: HttpLink, private apollo: Apollo, private cookie: CookieService) {
  }

  ngOnInit() {
    console.log(this.in_library, this.callback);
    // Set globals in EditorService //
    this.editor.authHost = this.authHost;
    this.editor.imageHost = this.imageHost;
    this.editor.videoHost = this.videoHost;
    this.editor.authKey = this.localAuthKey;
    this.editor.callback = this.callback;
    this.editor.graphQLResponse = this.graphQLResponse;
    this.editor.is_modal_open = false;
    this.editor.assetSource = this.assetSource;
    this.editor.user = this.user;
    if (this.asset) {
      this.editor.asset = this.asset;
      this.editor.addAsset(this.asset);
    }
    this.editor.unsplashKey = this.unsplashKey;
    this.editor.fbAppId = this.fbAppId;
    this.editor.in_library = this.in_library;
    // If unsplash or fbappid are missing, don't show buttons on selector //
    if (this.optionChecks()) {
      console.log(this.editor.authHost);
      let graphHosts: GraphHosts = {
        image: {
          link: this.imageHost + 'graphql',
          name: 'image',
          query: gql`{
              get {
              auth {
                status
              },
              resources {
                url,
                directory
              },
              status
            }
          }`
        },
        video: {
          link: this.videoHost + 'grapghql',
          name: 'video',
          query: gql`{
            videos {
              auth {
                status
              },
              resources {
                s3_path,
                old_path,
                error,
                directory,
                id
              }
            }
          }`
        }
      };

      this.editor.graphHosts = this.setupApollo(graphHosts);
      console.log('start', this.editor.graphHosts);

      this.editor.setUser().subscribe( (val) => {
        console.log(val);
      });
    }
  }
  // Gets errors for missing input params, and checks if require ones are present //
  optionChecks (): boolean {
    let errors = [];
    let has_requirements = true;
    if (!this.authHost) {
      errors.push('No auth host');
      has_requirements = false;
    }
    if (!this.imageHost) {
      errors.push('No image host');
      has_requirements = false;
    }
    if (!this.videoHost) {
      errors.push('No video host');
      has_requirements = false;
    }
    if (!this.unsplashKey) {
      errors.push('No Unsplash key');
    }
    if (!this.fbAppId) {
      errors.push('No Facebook App ID');
    }

    console.log(errors);

    return has_requirements;
  }

  setupApollo (graphHosts: GraphHosts): GraphHosts {
    console.log(graphHosts);
    if (Object.keys(graphHosts).length > 0) {
      const token = this.cookie.get(this.editor.authKey);
      let context = setContext(async (_, {headers}) => {
        // Grab token if there is one in storage or hasn't expired
        if (!token) {
          return {};
        }
        // Return the headers as usual
        return {
          headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
        };
      });

      Object.keys(graphHosts).forEach( (key: string) => {
        graphHosts[key].fetch = this.getWatchedQuery(graphHosts[key], context);
      });

      return graphHosts;
    }

    return null;
  }

  getWatchedQuery (host: GraphHost, context: any) {
    const imageLink = this.httpLink.create({
      uri: host.link
    });
    if (!this.apollo.use(host.name)) {
      const link = context;
      this.apollo.create({
        link: link.concat(imageLink),
        cache: new InMemoryCache()
      }, host.name);
    } else {
      console.log('has apollo client');
    }

    return this.apollo.use(host.name)
      .watchQuery({
        query: host.query,
      });
  }
}
