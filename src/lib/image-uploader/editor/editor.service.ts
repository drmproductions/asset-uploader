import {Observable, throwError} from 'rxjs/index';
import {HttpClient, HttpEvent, HttpEventType, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie';
import {catchError, map, last, tap} from 'rxjs/internal/operators';
import {ComponentRef, Injectable} from '@angular/core';
import {HttpService} from '../../http.service';
import {MatDialog} from '@angular/material';
import {AssetFile} from '../../interfaces/asset.file';
import {GraphHosts} from '../../interfaces/graph.hosts';
import {PulldownService} from "../pulldowns/pulldown.service";
import {MainmodalComponent} from "./mainmodal/mainmodal.component";

export interface Credentials {
  // Customize received credentials here
  username: string;
  token: string;
}

export interface User {
  first_name: string;
  last_name: string;
  phone_number: string;
  uid: string;
  email: string;
  refreshToken: string;
  firebase: any;
  username: string;
  organization: Organization;
  org_logged_as: number;
}

export interface Organization {
  name: string;
  id: number;
  role: string;
  role_id: number;
}

@Injectable()
export class EditorService extends HttpService {
  private _authHost: string;
  get authHost () { return this._authHost; }
  set authHost (authHost: string) { this._authHost = authHost; }

  private _imageHost: string;
  get imageHost () { return this._imageHost; }
  set imageHost (imageHost: string) { this._imageHost = imageHost; }

  private _videoHost: string;
  get videoHost () { return this._videoHost; }
  set videoHost (videoHost: string) { this._videoHost = videoHost; }

  private _authKey = 'token';
  get authKey () { return this._authKey; }
  set authKey (authKey: string) { this._authKey = authKey; }

  private _callback: Function;
  get callback () { return this._callback; }
  set callback (callback: Function) { this._callback = callback; }

  private _graphQLResponse: string;
  get graphQLResponse () { return this._graphQLResponse; }
  set graphQLResponse (graphQLResponse: string) { this._graphQLResponse = graphQLResponse; }

  private _is_modal_open: boolean;
  get is_modal_open () { return this._is_modal_open; }
  set is_modal_open (is_modal_open: boolean) { this._is_modal_open = is_modal_open; }

  private _asset: File | string | AssetFile;
  get asset () { return this._asset; }
  set asset (asset: File | string | AssetFile) { this._asset = asset; }

  private _assetType = '';
  get assetType () { return this._assetType; }
  set assetType (assetType: string) { this._assetType = assetType; }

  private _default_asset: File | string;
  get default_asset () { return this._default_asset; }
  set default_asset (default_asset: File | string) { this._default_asset = default_asset; }

  private _in_library: boolean;
  get in_library () { return this._in_library; }
  set in_library (in_library: boolean) { this._in_library = in_library; }

  private _unsplashKey: string;
  get unsplashKey () { return this._unsplashKey; }
  set unsplashKey (unsplashKey: string) { this._unsplashKey = unsplashKey; }

  private _fbAppId: string;
  get fbAppId () { return this._fbAppId; }
  set fbAppId (fbAppId: string) { this._fbAppId = fbAppId; }

  private _graphHosts: any;
  get graphHosts () { return this._graphHosts; }
  set graphHosts (graphHosts: GraphHosts) { this._graphHosts = graphHosts; }

  private _assetSource: any;
  get assetSource () { return this._assetSource; }
  set assetSource (assetSource: string) { this._assetSource = assetSource; }

  public user: User;

  constructor(public http: HttpClient, public cookie: CookieService, public dialog: MatDialog, private _pulldown: PulldownService) {
    super(http, cookie, dialog);

    console.log('editor constructor');
  }

  public getToken () {
      const value = '; ' + document.cookie;
      const parts = value.split(`; ${this.authKey}=`);
      if (parts.length === 2) {
        return parts.pop().split(';').shift();
      }
  }

  public upload (options: any): Observable<Object> {
    options.host = options.host ? options.host : this.imageHost;
    options.uploadCallback = (options.uploadCallback) ? options.uploadCallback : function () {};
    options.callback = (options.callback) ? options.callback : function () {};
    options.errorCallback = (options.errorCallback) ? options.errorCallback : function () {};
    const errors = this.getUploadErrors(options);
    if (errors) {
      return options.errorCallback(errors);
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.cookie.get(this.authKey)
      }),
      reportProgress: true,
      observe: 'events' as 'events',
      body: options.data
    };
    return this.http.request('POST', options.host + '/graphql', httpOptions).pipe(
      map( (event:  HttpEvent<any>) => this.handleResponse(event, options)),
      catchError(this.handleError(options.errorCallback)),
      last()
    );
  }

  public openModal () {
    // Hide freshchat bubble to avoid obscuring buttons //
    this.toggleFreshChatBubble(true);
    // If click on upload image button again, use orinally set asset //
    if (this.default_asset) {
      this.asset = this.default_asset;
    }
    // Flag modal to true to open //
    this.is_modal_open = true;
  }

  public closeSelector () {
    this.is_modal_open = false;
    this.toggleFreshChatBubble(false);
  }

  public closeEditor (instance: ComponentRef<any>, id: number) {
    this.asset = null;
    this._pulldown.removeComponentFromBody(instance, id);
  }

  public close (instance: ComponentRef<any>, id: number) {
    this.closeSelector();
    this.closeEditor(instance, id);
  }

  public addAsset (asset: string | File) {
    console.log('adding asset', asset);
    this.getAssetType(asset).then( (type: string) => {
      console.log('then', type);
      this.assetType = type;
    }).catch( (err: any) => {
      console.log(err);
    });
  }

  private getAssetType (asset: string | File): Promise<string> {
    console.log('adding asset');
    return new Promise<string>( (resolve: any, reject: any) => {
      this.asset = asset;

      if (asset instanceof File) {
        console.log('is file');
        if (!asset.type) {
          return reject('bad type:' + asset);
        }
        console.log('Checking file type', asset.type);
        resolve(this.getLibraryType(asset.type));
      } else if (typeof asset === 'string') {
        console.log('is string');
        this.http.head(asset, {observe: 'response'})
        .subscribe((response: any) => {
          if (!response) {
            return reject('No response');
          }
          console.log('heaeder 2', response.headers.get('content-type'));
          return resolve(this.getLibraryType(response.headers.get('content-type')));
        });
      }
    });
  }

  public isVideo (): boolean {
    return this.assetType.indexOf('video') !== -1;
  }

  public isImage (): boolean {
    return this.assetType.indexOf('image') !== -1;
  }

  public removeAsset () {
    this.asset = null;
    this.assetType = null;
  }

  setUser (): Observable<any> {
    console.log('setting user', this.authKey, this.cookie.get(this.authKey));
    return this.getUser(this.cookie.get(this.authKey))
      .pipe(
        catchError(this.handleError('setUser')),
        tap( (dbUser: any) => {
          console.log('found db user', dbUser);
          this.setCredentials(this.cookie.get(this.authKey), dbUser);
        })
      );
  }

  public setCredentials(token?: string, dbUser?: any) {
    console.log('setting credentilas');
    if (dbUser) {
      this.user = dbUser.user;
      // this.user.firebase = user;
      console.log(this.user);
      this.cookie.put(dbUser.token, token);
    } else {
      console.log('no user remove key');
      this.cookie.remove(this.authKey);
    }
  }

  getUser (token: string): Observable<Object> {
    const url = this.authHost + 'api/v1/verify';
    console.log(token, this.authHost);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.get<Object>(url, httpOptions)
      .pipe(
        catchError(this.handleError('getUser'))
      );
  }

  public handleError(operation: String) {
    return (err: any) => {
      const errMsg = `error in ${operation}()`;

      // this.openDialog(err);
      console.log(errMsg, err);

      return throwError('');
    };
  }

  private setToken(user: any, callback: Function) {
    user.getIdToken(true)
      .then((returnedToken: string) => {
        console.log('found token', returnedToken);
        callback(user, returnedToken);
      })
      .catch((error: any) => {
        console.log(error);
      });
  }

  private updateUserData(token: string, callback: Function): void {
    console.log('update user');
    console.log('token', document.cookie);
    console.log('user', this.user);
    const user = this.user;
    const query = {
      token: token,
      refreshToken: this.user.refreshToken,
      fbId: this.user.uid,
      email: this.user.email
    };
    this.call(this.authHost + 'instantiate', 'post', query)
      .subscribe((result: any) => {
        callback(result, user);
      });
  }

  private getLibraryType (type: string): string {
    if (type.indexOf('video') !== -1) {
      return 'video';
    } else if (type.indexOf('image') !== -1) {
      return 'image';
    }

    return 'n/a';
  }

  private toggleFreshChatBubble (hide: boolean) {
    const freshChatBubble = document.getElementById('fc_frame');
    // Check if fcBubble exists //
    if (freshChatBubble) {
      // If showing, remove z-index override property //
      if (!hide) {
        if (freshChatBubble.style.removeProperty) {
          freshChatBubble.style.removeProperty('z-index');
        } else {
          freshChatBubble.removeAttribute('z-index');
        }
      // if hiding, add zindex property to override set one //
      } else {
        freshChatBubble.style.zIndex = '0';
      }
    }
  }

  private handleResponse (xhrEvent: HttpEvent<any>, options: any) {
    console.log(xhrEvent);

    switch (xhrEvent.type) {
      case HttpEventType.Sent:
        console.log('Sent', xhrEvent);
        break;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        const percentDone = Math.round(100 * xhrEvent.loaded / xhrEvent.total);
        console.log( `File is ${percentDone}% uploaded.` );
        options.uploadCallback(percentDone);
        break;

      case HttpEventType.ResponseHeader:
        console.log('response header', xhrEvent);
        break;

      case HttpEventType.DownloadProgress:
        console.log('download response', xhrEvent);
        break;

      case HttpEventType.Response:
        console.log('response response', xhrEvent);
        // this.callback(xhrEvent.body);
        options.callback(xhrEvent.body);
        break;

      case HttpEventType.User:
        console.log('user response', xhrEvent);
        break;

      default:
        console.log('default response', xhrEvent);
        break;
    }

    return xhrEvent;
  }

  private getUploadErrors (options: any) {
   if (!options.data) {
      return 'Missing Data/GraphQL query';
    }
   if (!this.getToken()) {
      return 'Missing Token, possible incorrect cookie key';
    }

    return null;
  }
}
