import {Component, Input, Output} from '@angular/core';
import {EditorService} from '../editor.service';
import {PulldownService} from "../../pulldowns/pulldown.service";
import {MainmodalComponent} from "../mainmodal/mainmodal.component";

@Component({
    selector: 'app-upload-initiator',
    templateUrl: './initiatebutton.component.html',
    styleUrls: ['./initiatebutton.component.css'],
    host: {
        '(click)': 'toggleModal()'
    }
})
export class InitiatebuttonComponent {
    @Input() name = 'Upload File';

    constructor(private editor: EditorService, private _pulldown: PulldownService) {
    }

    toggleModal() {
        this.editor.openModal();
        this._pulldown.appendComponentToBody(MainmodalComponent, this._pulldown.ids.MAIN);
        this._pulldown.pulldowns[this._pulldown.ids.MAIN].instance.editor = this.editor;
        this._pulldown.pulldowns[this._pulldown.ids.MAIN].instance.pulldown = this._pulldown;
    }
}
