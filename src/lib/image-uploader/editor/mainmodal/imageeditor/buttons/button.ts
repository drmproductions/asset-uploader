export abstract class Button {

    constructor() {}

    closeHiddenMenus () {
        const controls = document.querySelector('.tui-image-editor-controls')[0];
        const hiddenMenus = controls.querySelectorAll('.hiddenmenu');
        for (let a = 0; a !== hiddenMenus.length; a++) {
            hiddenMenus[a].style.display = 'none';
        }
    }

    // Menu item buttons after choosing main //
    closeSubMenus () {
        const controls = document.querySelector('.tui-image-editor-controls')[0];
        const subMenus = controls.querySelectorAll('.submenu');
        for (let a = 0; a !== subMenus.length; a++) {
            subMenus[a].style.display = 'none';
        }
    }
}
