import {Component, Input} from '@angular/core';
import {Button} from '../button';
import {ButtonInterface} from '../button.interface';
import {ImageeditorComponent} from '../../imageeditor.component';

@Component({
    selector: 'app-rotate-button',
    templateUrl: './rotate.component.html',
    styleUrls: ['./rotate.component.css']
})
export class RotateComponent extends Button implements ButtonInterface {

    @Input() angle: number;

    constructor (public editor: ImageeditorComponent) {
        super();
    }

    toggle (imageEditor: any, input?: HTMLInputElement) {
        imageEditor.rotate(this.angle).then( () => {
        }).catch( (e: any) => {
            console.log(e);
        });
    }

    set () {}
}
