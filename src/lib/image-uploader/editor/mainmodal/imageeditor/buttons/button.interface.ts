export interface ButtonInterface {
    toggle(imageEditor: any, input?: HTMLInputElement): void;
    set(imageEditor: any, input: HTMLInputElement): void;
}
