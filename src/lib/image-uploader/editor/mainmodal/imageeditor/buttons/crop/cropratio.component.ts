import {Component, Input} from '@angular/core';
import {Button} from '../button';
import {ButtonInterface} from '../button.interface';
import {ImageeditorComponent} from '../../imageeditor.component';

@Component({
    selector: 'app-crop-ratio-button',
    templateUrl: './cropratio.component.html',
    styleUrls: ['./cropratio.component.css']
})
export class CropratioComponent extends Button implements ButtonInterface {

    @Input() width: number;
    @Input() height: number;

    constructor (public editor: ImageeditorComponent) {
        super();
    }

    toggle (imageEditor: any, input?: HTMLInputElement) {
        if (imageEditor.getDrawingMode() !== 'CROPPER') {
            imageEditor.startDrawingMode('CROPPER');
        }
        console.log(this.width / this.height);
        imageEditor.setCropzoneRect(this.width / this.height);
    }

    set () {}
}
