import {Component, Input} from '@angular/core';
import {Button} from '../button';
import {ButtonInterface} from '../button.interface';
import {ImageeditorComponent} from '../../imageeditor.component';

interface Quad {
    top: number;
    left: number;
    width: number;
    height: number;
}

@Component({
    selector: 'app-crop-apply-button',
    templateUrl: './cropapply.component.html',
    styleUrls: ['./cropapply.component.css']
})
export class CropapplyComponent extends Button implements ButtonInterface {

    constructor (public editor: ImageeditorComponent) {
        super();
    }

    toggle (imageEditor: any, input?: HTMLInputElement) {}

    set (imageEditor: any) {
        console.log(imageEditor.getCropzoneRect());
        if (!this.is_too_small(imageEditor.getCropzoneRect())) {
            imageEditor.crop(imageEditor.getCropzoneRect()).then( (result: any) => {
                imageEditor.stopDrawingMode();
                this.closeHiddenMenus();
            }).catch( (e: any) => {
                console.log(e);
            });
        }
    }

    is_too_small (rect: Quad): boolean {
        if (!rect) {
            return true;
        }

        return (rect.width < 2 || rect.height < 2);
    }
}
