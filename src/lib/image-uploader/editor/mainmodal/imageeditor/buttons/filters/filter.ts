import {Button} from '../button';

export abstract class Filter extends Button {
    id: string;

    abstract set(imageEditor: any, input: HTMLInputElement): void;

    apply (imageEditor: any, options: Object = null, callback?: Function) {
        imageEditor.applyFilter(this.id, options).then( (result: any) => {
            if (callback) {
                callback(result);
            }
        });
    }

    is_applied (imageEditor: any) {
        return imageEditor.hasFilter(this.id);
    }
}
