import {Component} from '@angular/core';
import {Filter} from '../filter';
import {ImageeditorComponent} from '../../../imageeditor.component';
import {ButtonInterface} from '../../button.interface';

@Component({
    selector: 'app-brightness-button',
    templateUrl: './brightness.component.html',
    styleUrls: ['./brightness.component.css']
})
export class BrightnessComponent extends Filter implements ButtonInterface {

    id = 'brightness';

    constructor (public editor: ImageeditorComponent) {
        super();
    }

    toggle (imageEditor: any, input: any) {
        if (this.is_applied(imageEditor)) {
            imageEditor.removeFilter(this.id).then( (result: any) => {
                console.log(result);
                this.closeHiddenMenus();
            });
        } else {
            this.apply(imageEditor, {
                brightness: parseInt(input.value, 10)
            });
        }
    }

    set (imageEditor: any, input: any) {
        this.apply(imageEditor, {
            brightness: parseInt(input.value, 10)
        });
    }

    brightnessLabel(value: number | null) {
        if (!value) {
            return 0;
        }

        return value;
    }
}
