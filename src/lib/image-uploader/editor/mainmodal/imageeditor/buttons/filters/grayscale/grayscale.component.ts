import {Component} from '@angular/core';
import {Filter} from '../filter';
import {ImageeditorComponent} from '../../../imageeditor.component';
import {ButtonInterface} from '../../button.interface';

@Component({
    selector: 'app-grayscale-button',
    templateUrl: './grayscale.component.html',
    styleUrls: ['./grayscale.component.css']
})
export class GrayscaleComponent extends Filter implements ButtonInterface {

    id = 'Grayscale';

    constructor (public editor: ImageeditorComponent) {
        super();
    }

    toggle (imageEditor: any) {
        if (this.is_applied(imageEditor)) {
            imageEditor.removeFilter(this.id).then( (result: any) => {
                console.log(result);
            });
        } else {
            imageEditor.applyFilter(this.id).then( (result: any) => {
                console.log(result);
            });
        }
    }

    set (imageEditor: any) {}
}
