import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, ViewEncapsulation, Input} from '@angular/core';
import * as _$ from 'jquery';
import {EditorService} from '../../editor.service';
import {ScriptLoaderService} from '../../scriptLoaderService';
import * as fabric from '../../fabric/dist/fabric';
import * as ImageEditor from '../../tuieditor/tui-image-editor';
import * as FileSaver from 'file-saver';
import 'hammerjs';
import {PulldownService} from '../../../pulldowns/pulldown.service';
import {FinalStepPullDown} from '../../../pulldowns/finalstep/finalStep';

declare global {
    interface Window {
        File: File;
        FileList: FileList;
        FileReader: FileReader;
    }
}

interface FileOptions {
    name: string;
    directory: string;
    isLibraryAsset: boolean;
}

declare var tui: any;
declare var require: any;
const ColorPicker = require('tui-color-picker');
const $ = _$;

@Component({
    selector: 'app-image-editor',
    templateUrl: './imageeditor.component.html',
    styleUrls: ['./imageeditor.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ImageeditorComponent implements AfterViewInit, OnDestroy {

    undos = 0;
    redos = 0;
    supportingFileAPI: boolean;
    imageEditor: any;
    rImageType = /data:(image\/.+);base64,/;
    is_image_loading = false;
    MAX_RESOLUTION = 3264 * 2448; // 8MP (Mega Pixel)

    @Input() editor: EditorService;
    @Input() pulldown: PulldownService;

    constructor(private loader: ScriptLoaderService, private ref: ChangeDetectorRef) {
        if (!fabric) {
            console.log('fabric required');
        }
        // Defines variable tui //
        if (typeof tui === 'undefined' || !tui) {
          console.log('here');
            this.loader.load({src: 'https://uicdn.toast.com/tui.code-snippet/v1.5.0/tui-code-snippet.min.js', name: 'snippet'})
                .pipe(
                    // switchMap((result, index) => {
                    //     console.log(tui);
                    //     // requires fabric and tui snippet, expects variable tui { colorPicker, utils } //
                    //     tui.colorPicker = ColorPicker;
                    //     // Adds ImageEditor to tui //
                    //     return this.loader.load({src: 'https://uicdn.toast.com/tui-image-editor/v3.3.0/tui-image-editor.js',
                    //         name: 'tui'});
                    // })
                )
                .subscribe(() => {
                  tui.colorPicker = ColorPicker;
                  tui.ImageEditor = ImageEditor;
                  console.log('load', tui);
                    console.log('a');
                    this.loadMobile();
                });
        }
    }

    ngAfterViewInit () {
        if (typeof tui !== 'undefined') {
            this.loadMobile();
        }
    }

    ngOnDestroy () {
      console.log('destroying');
      // document.getElementById("btn-redo").removeEventListener("mousemove", myFunction);
    }

    onClose (e: any) {
      console.log('Close button hit');
        this.editor.close(this.pulldown.getRef(this.pulldown.ids.MAIN), this.pulldown.ids.MAIN);
    }

    loadMobile () {
        /**
         * mobile.js
         * @author NHN Ent. FE Development Team <dl_javascript@nhnent.com>
         * @fileoverview
         */
        /* eslint-disable consts-on-top */
        this.supportingFileAPI = !!(window.File && window.FileList && window.FileReader);
        const shapeOpt = {
            fill: '#fff',
            stroke: '#000',
            strokeWidth: 10
        };
        let activeObjectId: any;

        // Selector of image editor controls
        const submenuClass = '.submenu';
        const hiddenmenuClass = '.hiddenmenu';

        const $controls = $('.tui-image-editor-controls');
        const $menuButtons = $controls.find('.menu-button');
        const $submenuButtons = $controls.find('.submenu-button');
        const $btnShowMenu = $controls.find('.btn-prev');
        const $msg = $controls.find('.msg');

        const $subMenus = $controls.find(submenuClass);
        const $hiddenMenus = $controls.find(hiddenmenuClass);

        // Image editor controls - top menu buttons
        const $btnUndo = $('#btn-undo');
        const $btnRedo = $('#btn-redo');
        const $btnRemoveActiveObject = $('#btn-remove-active-object');

        // Image editor controls - bottom menu buttons
        const $btnAddText = $('#btn-add-text');

        // Image editor controls - bottom submenu buttons
        const $btnFlipX = $('#btn-flip-x');
        const $btnFlipY = $('#btn-flip-y');
        const $btnRotateClockwise = $('#btn-rotate-clockwise');
        const $btnRotateCounterClockWise = $('#btn-rotate-counter-clockwise');
        const $btnAddArrowIcon = $('#btn-add-arrow-icon');
        const $btnAddCancelIcon = $('#btn-add-cancel-icon');
        const $btnAddCustomIcon = $('#btn-add-custom-icon');
        const $btnFreeDrawing = $('#btn-free-drawing');
        const $btnLineDrawing = $('#btn-line-drawing');
        const $btnAddRect = $('#btn-add-rect');
        const $btnAddSquare = $('#btn-add-square');
        const $btnAddEllipse = $('#btn-add-ellipse');
        const $btnAddCircle = $('#btn-add-circle');
        const $btnAddTriangle = $('#btn-add-triangle');
        const $btnChangeTextStyle = $('.btn-change-text-style');

        // Image editor controls - etc.
        const $inputTextSizeRange = $('#input-text-size-range');
        const $inputBrushWidthRange = $('#input-brush-range');
        const $inputStrokeWidthRange = $('#input-stroke-range');
        const $inputCheckTransparent = $('#input-check-transparent');

        // Colorpicker
        const iconColorpicker = tui.colorPicker.create({
            container: $('#tui-icon-color-picker')[0],
            color: '#000000'
        });

        const textColorpicker = tui.colorPicker.create({
            container: $('#tui-text-color-picker')[0],
            color: '#000000'
        });

        const brushColorpicker = tui.colorPicker.create({
            container: $('#tui-brush-color-picker')[0],
            color: '#000000'
        });

        const shapeColorpicker = tui.colorPicker.create({
            container: $('#tui-shape-color-picker')[0],
            color: '#000000'
        });

        // Create image editor
        const imageEditor = new tui.ImageEditor('.tui-image-editor', {
            cssMaxWidth: document.documentElement.clientWidth,
            cssMaxHeight: document.documentElement.clientHeight,
            selectionStyle: {
                cornerSize: 10,
                rotatingPointOffset: 20
            }
        });
        this.imageEditor = imageEditor;

        let $displayingSubMenu: any, $displayingHiddenMenu: any;

        function hexToRGBa(hex: string, alpha: number) {
            const r = parseInt(hex.slice(1, 3), 16);
            const g = parseInt(hex.slice(3, 5), 16);
            const b = parseInt(hex.slice(5, 7), 16);
            const a = alpha || 1;

            return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')';
        }

        function getBrushSettings() {
            const brushWidth = $inputBrushWidthRange.val();
            const brushColor = brushColorpicker.getColor();

            return {
                width: brushWidth,
                color: hexToRGBa(brushColor, 0.5)
            };
        }

        function activateShapeMode() {
            imageEditor.stopDrawingMode();
        }

        function activateIconMode() {
            imageEditor.stopDrawingMode();
        }

        function activateTextMode() {
            if (imageEditor.getDrawingMode() !== 'TEXT') {
                imageEditor.stopDrawingMode();
                imageEditor.startDrawingMode('TEXT');
            }
        }

        function setTextToolbar(obj: any) {
            const fontSize = obj.fontSize;
            const fontColor = obj.fill;

            $inputTextSizeRange.val(fontSize);
            textColorpicker.setColor(fontColor);
        }

        function setIconToolbar(obj: any) {
            const iconColor = obj.fill;

            iconColorpicker.setColor(iconColor);
        }

        function setShapeToolbar(obj: any) {
            let strokeColor, fillColor, isTransparent;
            const colorType = $('[name="select-color-type"]:checked').val();

            if (colorType === 'stroke') {
                strokeColor = obj.stroke;
                isTransparent = (strokeColor === 'transparent');

                if (!isTransparent) {
                    shapeColorpicker.setColor(strokeColor);
                }
            } else if (colorType === 'fill') {
                fillColor = obj.fill;
                isTransparent = (fillColor === 'transparent');

                if (!isTransparent) {
                    shapeColorpicker.setColor(fillColor);
                }
            }

            $inputCheckTransparent.prop('checked', isTransparent);
            $inputStrokeWidthRange.val(obj.strokeWith);
        }

        function showSubMenu(type: string) {
            let index;

            switch (type) {
                case 'shape':
                    index = 3;
                    break;
                case 'icon':
                    index = 4;
                    break;
                case 'text':
                    index = 5;
                    break;
                default:
                    index = 0;
            }

            $displayingSubMenu.hide();
            $displayingHiddenMenu.hide();

            $displayingSubMenu = $menuButtons.eq(index).parent().find(submenuClass).show();
        }

        // const MAX_RESOLUTION = this.MAX_RESOLUTION;
        // function loadImage (event: any) {
        //     let file;
        //     let img;
        //     let resolution;
        //
        //     // if (!this.supportingFileAPI) {
        //     //     alert('This browser does not support file-api');
        //     // }
        //
        //     file = event.target.files[0];
        //     if (file) {
        //         img = new Image();
        //
        //         img.onload = function() {
        //             resolution = this.width * this.height;
        //
        //             if (resolution <= MAX_RESOLUTION) {
        //                 imageEditor.loadImageFromFile(file).then(() => {
        //                     imageEditor.clearUndoStack();
        //                 });
        //             } else {
        //                 alert('Loaded image\'s resolution is too large!\nRecommended resolution is 3264 * 2448!');
        //             }
        //
        //             URL.revokeObjectURL(file);
        //         };
        //
        //         img.src = URL.createObjectURL(file);
        //     }
        // }
        // document.getElementById('input-image-file').addEventListener('change', (e) => {
        //     loadImage(e);
        // });

        // Bind custom event of image editor
        const self = this;
        imageEditor.on({
            undoStackChanged: function(length: number) {
                if (length) {
                  $btnUndo.removeClass('disabled');
                } else {
                  $btnUndo.addClass('disabled');
                }
                self.setDos(imageEditor);
            },
            redoStackChanged: function(length: number) {
                if (length) {
                  $btnRedo.removeClass('disabled');
                } else {
                  $btnRedo.addClass('disabled');
                }
                self.setDos(imageEditor);
            },
            objectScaled: function(obj: any) {
                if (obj.type === 'text') {
                    $inputTextSizeRange.val(obj.fontSize);
                }
            },
            objectActivated: function(obj: any) {
                activeObjectId = obj.id;
                if (obj.type === 'rect' || obj.type === 'circle' || obj.type === 'triangle') {
                    showSubMenu('shape');
                    setShapeToolbar(obj);
                    activateShapeMode();
                } else if (obj.type === 'icon') {
                    showSubMenu('icon');
                    setIconToolbar(obj);
                    activateIconMode();
                } else if (obj.type === 'text') {
                    showSubMenu('text');
                    setTextToolbar(obj);
                    activateTextMode();
                }
            }
        });

        // Image editor controls action
        $menuButtons.on('click', function() {
            $displayingSubMenu = $(this).parent().find(submenuClass).show();
            $displayingHiddenMenu = $(this).parent().find(hiddenmenuClass);
        });

        $submenuButtons.on('click', function() {
            $displayingHiddenMenu.hide();
            $displayingHiddenMenu = $(this).parent().find(hiddenmenuClass).show();
        });

        $btnShowMenu.on('click', function() {
            $displayingSubMenu.hide();
            $displayingHiddenMenu.hide();
            $msg.show();

            if (imageEditor.hasFilter('Grayscale')) {
                imageEditor.removeFilter('Grayscale');
            }

            imageEditor.stopDrawingMode();
        });

        // Undo action
      console.log('------ Binding buttons --------');
        // $btnUndo.on('click', function(event: any) {
        //   console.log('clicked undo', $(this).hasClass('disabled'));
        //     if (!$(this).hasClass('disabled')) {
        //         imageEditor.undo().then( (result: any) => {
        //           console.log('undo', result);
        //         }).catch( (e: any) => {
        //           console.log('undo', e);
        //         });
        //     }
        // });
        //
        // // Redo action
        // $btnRedo.on('click', function(event: any) {
        //   console.log('clicked redo', $(this).hasClass('disabled'));
        //     if (!$(this).hasClass('disabled')) {
        //         imageEditor.redo().then( (result: any) => {
        //           console.log('redo', result);
        //         }).catch( (e: any) => {
        //           console.log('redo', e);
        //         });
        //     }
        // });

        // Remove active object action
        $btnRemoveActiveObject.on('click', function() {
            if (activeObjectId) {
                imageEditor.removeObject(activeObjectId);
                activeObjectId = null;
            }
        });

        // Crop menu action
        document.querySelector('#btn-crop').addEventListener('click', () => {
            imageEditor.startDrawingMode('CROPPER');
        });

        // Orientation menu action
        $btnRotateClockwise.on('click', function() {
            imageEditor.rotate(90).then( (result: any) => {
              console.log('fin');
            }).catch( ( e: any ) => {
              console.log('err', e);
            });
        });

        $btnRotateCounterClockWise.on('click', function() {
            imageEditor.rotate(-90).then( (result: any) => {
              console.log('fin -90');
            }).catch( ( e: any ) => {
              console.log('err -90', e);
            });
        });

        $btnFlipX.on('click', function() {
            imageEditor.flipX();
        });

        $btnFlipY.on('click', function() {
            imageEditor.flipY();
        });

        // Icon menu action
        $btnAddArrowIcon.on('click', function() {
            imageEditor.addIcon('arrow');
        });

        $btnAddCancelIcon.on('click', function() {
            imageEditor.addIcon('cancel');
        });

        $btnAddCustomIcon.on('click', function() {
            imageEditor.addIcon('customArrow');
        });

        iconColorpicker.on('selectColor', function(event: any) {
            imageEditor.changeIconColor(activeObjectId, event.color);
        });

        // Text menu action
        $btnAddText.on('click', function() {
            const initText = 'DoubleClick';
            const val = $inputTextSizeRange.val() as string;

            imageEditor.startDrawingMode('TEXT');
            imageEditor.addText(initText, {
                styles: {
                    fontSize: parseInt(val, 10)
                }
            });
        });

        $btnChangeTextStyle.on('click', function() {
            const styleType = $(this).attr('data-style-type');
            const styleObj = {};
            let styleObjKey;

            switch (styleType) {
                case 'bold':
                    styleObjKey = 'fontWeight';
                    break;
                case 'italic':
                    styleObjKey = 'fontStyle';
                    break;
                case 'underline':
                    styleObjKey = 'textDecoration';
                    break;
                case 'left':
                    styleObjKey = 'textAlign';
                    break;
                case 'center':
                    styleObjKey = 'textAlign';
                    break;
                case 'right':
                    styleObjKey = 'textAlign';
                    break;
                default:
                    styleObjKey = '';
            }

            styleObj[styleObjKey] = styleType;

            imageEditor.changeTextStyle(activeObjectId, styleObj);
        });

        $inputTextSizeRange.on('change', function() {
            const val = $(this).val() as string;
            imageEditor.changeTextStyle(activeObjectId, {
                fontSize: parseInt(val, 10)
            });
        });

        textColorpicker.on('selectColor', function(event: any) {
            imageEditor.changeTextStyle(activeObjectId, {
                fill: event.color
            });
        });

        // Draw line menu action
        $btnFreeDrawing.on('click', function() {
            const settings = getBrushSettings();

            imageEditor.stopDrawingMode();
            imageEditor.startDrawingMode('FREE_DRAWING', settings);

            const subButtons = document.getElementsByClassName('submenu-button');
            for (let a = 0; a !== subButtons.length; a++) {
                subButtons[a].classList.remove('tui-selected-btn');
            }

            $btnFreeDrawing.addClass('tui-selected-btn');
        });

        $btnLineDrawing.on('click', function() {
            const settings = getBrushSettings();

            imageEditor.stopDrawingMode();
            imageEditor.startDrawingMode('LINE_DRAWING', settings);

            const subButtons = document.getElementsByClassName('submenu-button');
            for (let a = 0; a !== subButtons.length; a++) {
                subButtons[a].classList.remove('tui-selected-btn');
            }

            $btnLineDrawing.addClass('tui-selected-btn');
        });

        $inputBrushWidthRange.on('change', function() {
            const val = $(this).val() as string;
            imageEditor.setBrush({
                width: parseInt(val, 10)
            });
        });

        brushColorpicker.on('selectColor', function(event: any) {
            imageEditor.setBrush({
                color: hexToRGBa(event.color, 0.5)
            });
        });

        // Add shape menu action
        $btnAddRect.on('click', function() {
            imageEditor.addShape('rect', tui.util.extend({
                width: 50,
                height: 30
            }, shapeOpt));
        });

        $btnAddSquare.on('click', function() {
            imageEditor.addShape('rect', tui.util.extend({
                width: 50,
                height: 50,
                isRegular: true
            }, shapeOpt));
        });

        $btnAddEllipse.on('click', function() {
            imageEditor.addShape('circle', tui.util.extend({
                rx: 30,
                ry: 20
            }, shapeOpt));
        });

        $btnAddCircle.on('click', function() {
            imageEditor.addShape('circle', tui.util.extend({
                rx: 20,
                ry: 20,
                isRegular: true
            }, shapeOpt));
        });

        $btnAddTriangle.on('click', function() {
            imageEditor.addShape('triangle', tui.util.extend({
                width: 50,
                height: 40,
                isRegular: true
            }, shapeOpt));
        });

        $inputStrokeWidthRange.on('change', function() {
            const val = $(this).val() as string;
            imageEditor.changeShape(activeObjectId, {
                strokeWidth: parseInt(val, 10)
            });
        });

        $inputCheckTransparent.on('change', function() {
            const colorType = $('[name="select-color-type"]:checked').val();
            const isTransparent = $(this).prop('checked');
            let color;

            if (!isTransparent) {
                color = shapeColorpicker.getColor();
            } else {
                color = 'transparent';
            }

            if (colorType === 'stroke') {
                imageEditor.changeShape(activeObjectId, {
                    stroke: color
                });
            } else if (colorType === 'fill') {
                imageEditor.changeShape(activeObjectId, {
                    fill: color
                });
            }
        });

        shapeColorpicker.on('selectColor', function(event: any) {
            const colorType = $('[name="select-color-type"]:checked').val();
            const isTransparent = $inputCheckTransparent.prop('checked');
            const color = event.color;

            if (isTransparent) {
                return;
            }

            if (colorType === 'stroke') {
                imageEditor.changeShape(activeObjectId, {
                    stroke: color
                });
            } else if (colorType === 'fill') {
                imageEditor.changeShape(activeObjectId, {
                    fill: color
                });
            }
        });

        // Load image
        console.log(this.editor.asset);
        if (this.editor.asset instanceof File) {
            imageEditor.loadImageFromFile(this.editor.asset, 'SampleImage').then((result: any) => {
                console.log(result);
                imageEditor.clearUndoStack();
            }).catch( (e: any) => {
                console.log(e);
            });
        } else {
            imageEditor.loadImageFromURL(this.editor.asset, 'SampleImage').then((result: any) => {
                console.log(result);
                imageEditor.clearUndoStack();
            }).catch( (e: any) => {
                console.log(e);
            });
        }
    }

    setDos (imageEditor: any) {
        console.log('setting dos', imageEditor._invoker);
        this.undos = imageEditor._invoker._undoStack.length;
        this.redos = imageEditor._invoker._redoStack.length;
        this.ref.detectChanges();
    }

    uploadImage (data: FileOptions) {
      let imageName = this.imageEditor.getImageName();
      const dataURL = this.imageEditor.toDataURL();
      const blob = this.base64ToBlob(dataURL);
      console.log(blob);
      const type = blob.type.split('/')[1];
      if (imageName.split('.').pop() !== type) {
        imageName += '.' + type;
      }
      const a = this.blobToFile(blob, imageName);

      this.editor.close(this.pulldown.getRef(this.pulldown.ids.MAIN), this.pulldown.ids.MAIN);

      this.editor.upload({data: this.getFileUploadQuery(a, data)})
        .subscribe((result: any) => {
          console.log('closing editor', this.editor);
          if (this.editor.callback) {
            console.log('has callback', result.body.data.add, this.editor.assetSource);
            this.editor.callback(result.body.data.add, this.editor.assetSource);
          }
        });
    }

    downloadImage (event: any) {
        let imageName = this.imageEditor.getImageName();
        const dataURL = this.imageEditor.toDataURL();
        let blob, type, w;

        if (this.supportingFileAPI) {
            blob = this.base64ToBlob(dataURL);
            type = blob.type.split('/')[1];
            if (imageName.split('.').pop() !== type) {
                imageName += '.' + type;
            }

            // Library: FileSaver - saveAs
            FileSaver.saveAs(blob, imageName); // eslint-disable-line
        } else {
            alert('This browser needs a file-server');
            w = window.open();
            w.document.body.innerHTML = '<img src=' + dataURL + '>';
        }
    }

    openFinalStep () {
        this.pulldown.appendComponentToBody(FinalStepPullDown, this.pulldown.ids.FINAL);
        this.pulldown.pulldowns[this.pulldown.ids.FINAL].instance.finish.subscribe( (data: FileOptions) => {
            console.log(data);
            this.uploadImage(data);
        });
    }

    getFileUploadQuery(file: File, fileOptions: FileOptions = {name: '', directory: '', isLibraryAsset: false}): FormData {
        const formData: FormData = new FormData();
        formData.append('0', file);
        formData.append('map', '{"0": ["variables.file0"]}');

        const fileName = fileOptions.name ? fileOptions.name : file.name;
        const directory = fileOptions.directory ? fileOptions.directory : '';
        const isLibraryAsset = fileOptions.isLibraryAsset ? fileOptions.isLibraryAsset : false;

        let options = '';
        options = options ? ',' + options : '';
        const mutations = `mutation($file0: Upload)`;
        const transformations = `{ add( transformations:[{image: $file0 ${options} }], is_library: ${isLibraryAsset}, directory: \\"${directory}\\", name: \\"${fileName}\\")`;
        const variables = ', "variables": {' + '"file0":[null]' + '}';
        const query = `${mutations} ${transformations} { auth{status,token}, resources{url,directory,name} }`;
        const operations = '{ "query": "' + query + '}"' + variables + '}';
        formData.append('operations', operations);
        console.log('operations', operations, this.editor.in_library);

        return formData;
    }

    base64ToBlob(data: string) {
        let mimeString = '';
        let raw, uInt8Array, rawLength;

        raw = data.replace(this.rImageType, function(header: string, imageType: string) {
            mimeString = imageType;

            return '';
        });

        raw = atob(raw);
        rawLength = raw.length;
        uInt8Array = new Uint8Array(rawLength); // eslint-disable-line

        for (let i = 0; i < rawLength; i += 1) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: mimeString});
    }

    blobToFile (theBlob: Blob, fileName: string): File {
        const b: any = theBlob;
        // A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;

        // Cast to a File() type
        return <File>theBlob;
    }

    goBack () {
      console.log('hit back button');
      console.log(this.editor.asset);
        this.editor.removeAsset();
      console.log(this.editor.asset);
    }

    imageEditorFunction (callback: Function): void {
      if (!this.is_image_loading) {
        callback();
      }
    }

    undo (imageEditor: any) {
      console.log('clicked undo', $(this).hasClass('disabled'));
      if (!$(this).hasClass('disabled')) {
        imageEditor.undo().then( (result: any) => {
          console.log('undo', result);
        }).catch( (e: any) => {
          console.log('undo', e);
        });
      }
    }

    redo (imageEditor: any) {
      console.log('clicked redo', $(this).hasClass('disabled'));
      if (!$(this).hasClass('disabled')) {
        imageEditor.redo().then( (result: any) => {
          console.log('redo', result);
        }).catch( (e: any) => {
          console.log('redo', e);
        });
      }
    }
}
