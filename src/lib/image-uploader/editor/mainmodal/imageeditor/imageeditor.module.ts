import { NgModule } from '@angular/core';
import {MatBadgeModule, MatIconModule, MatSliderModule} from '@angular/material';
import {CropapplyComponent} from './buttons/crop/cropapply.component';
import {CropratioComponent} from './buttons/crop/cropratio.component';
import {RotateComponent} from './buttons/rotate/rotate.component';
import {GrayscaleComponent} from './buttons/filters/grayscale/grayscale.component';
import {BrightnessComponent} from './buttons/filters/brightness/brightness.component';
import {ImageeditorComponent} from './imageeditor.component';
import {SubmitbuttonComponent} from '../canvascontainers/canvascontainer/submitbutton/submitbutton.component';

@NgModule({
  imports: [
    MatIconModule,
    MatBadgeModule,
    MatSliderModule
  ],
  declarations: [
    RotateComponent,
    SubmitbuttonComponent,
    CropratioComponent,
    CropapplyComponent,
    ImageeditorComponent,
    GrayscaleComponent,
    BrightnessComponent
  ],
  providers: [
  ],
  exports: [
    ImageeditorComponent
  ],
  entryComponents: [ ]
})
export class ImageeditorModule { }
