import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef, Input,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {Option} from '../canvascontainers/canvascontainer/optionscontainer/option';
import {EditorService} from '../../../editor/editor.service';
import {ItemInterface} from '../canvascontainers/canvascontainer/item/item.interface';
import {ItemComponent} from '../canvascontainers/canvascontainer/item/item.component';

declare var $: any;

@Component({
    selector: 'app-video-editor',
    templateUrl: './videoeditor.component.html',
    styleUrls: ['./videoeditor.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class VideoeditorComponent extends ItemComponent implements ItemInterface, OnInit, AfterViewInit {
    public name = 'video_';
  public id: number;
  public redos: any;
  public undos: any;
  public sourceUrl: SafeUrl;
  @Input() file: File;
  @ViewChild('myVideo') myVideo: ElementRef;
    private widthPercent = 90; // canvas internal padding (non-css) //
    private heightPercent = 90; // canvas internal padding (non-css) //

    constructor(private sanitizer: DomSanitizer, private editor: EditorService, private ref: ChangeDetectorRef) {
        super();
        console.log('started video');
    }

    onResize ( e: any ): void {
        this.setElementSize(window.innerWidth, window.innerHeight);
    }

    // When clicking on video player //
    onClick ( e: any ) {
      console.log('clicked on video player');
        if (e.target.paused) {
            e.target.currentTime = this.getMinTime();
            e.target.play();
        } else {
          e.target.pause();
        }
    }

    // When video is playing content, stop it if it goes out of range //
    onPlaying ( e: any ) {
      console.log('video playing');
        if (!e.target.paused) {
            if (e.target.currentTime >= this.getMaxTime()) {
              e.target.pause();
            }
        }
    }

    onProgress (e: any) {
      console.log('on progress');
        // this.element.load();
        this.hideLoading();
    }

    ngOnInit () {
        this.element = this.myVideo.nativeElement;
        this.host = this.editor.videoHost;

        const hasAudio = this.hasAudio;
        const options = this.options;
        const element = this.element;
        console.log(this.element, this.host, this.options);
        this.element.addEventListener('loadedmetadata', function() {
            console.log('load meta');
            if (options.trim) {
              options.trim.setup();
            }
        });

        console.log(this.element.readyState);

        this.element.addEventListener('loadeddata', function() {
          console.log('loading video data');
            if (hasAudio($(element).get(0))) {
              options.mute.enable();
            } else {
              options.mute.disable();
            }
        });
    }

    ngAfterViewInit(): void {
      console.log('stretching');
        // Stretch video to screen size //
        this.setElementSize(window.innerWidth, window.innerHeight);
        // use generated url from upload object and set it to video src //
        this.upload();
    }

    heartbeat(id: number, token: string, video: VideoeditorComponent) {
      console.log('video heartbeat');
        const data = {id: id};
        setTimeout( function() {
            $.ajax({
                type: 'POST',
                url: video.host + '/status',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                headers: {'Authorization': `Bearer ${token}`},
                success: (result: any) => {
                    console.log(result);
                    const error = (result.error) ? ': ' + result.error : '';
                    const type = (error) ? video.messageTypes.error : video.messageTypes.success;
                    video.setMessage(result.status + error, type);
                    video.setProgressBar(50 + Math.round(result.progress) / 2);
                    if (result.url && !error) {
                        video.hideLoading();
                        video.addSource(result.url);
                    } else {
                        if (!error) {
                          video.heartbeat(id, token, video);
                        }
                    }
                }
            });
        }, 1000);
    }

    getElementScale (): {width: number, height: number} {
        return {
            width: this.element.width * (this.widthPercent / 100),
            height: this.element.height * (this.heightPercent / 100)
        };
    }

    setElementSize (width: number, height: number) {
        this.element.width = width * (this.widthPercent / 100);
        this.element.height = height * (this.heightPercent / 100);

        return this.element;
    }

    reset () {
      console.log('resetting');
        this.element.pause();
        this.element.currentTime = 0;
    }

    // Any error thrown while uploading video //
    handleError(e: any) {
        // const itemModal = $(this.getElement()).siblings('.item-loading-modal').find('.item-loading-message');
        // itemModal.append(`<div>${e.target.error}</div>`);
        console.log(e.target.error);
    }

    getMinTime (): number {
        return (this.options.trim) ? this.options.trim.getMinTime() : 0;
    }

    getMaxTime (): number {
        return (this.options.trim) ? this.options.trim.getMaxTime() : this.element.duration;
    }

    addSource (url: string): void {
      console.log('adding source');
      this.sourceUrl = this.sanitizer.bypassSecurityTrustUrl(decodeURIComponent(url));
      console.log($(this.element).get(0));
      $(this.element).get(0).load();
      this.ref.detectChanges();
    }

    upload () {
      console.log('start upload');
        this.showLoading();

        const data = this.getFormData();
        const heartbeat = this.heartbeat;
        const token = this.editor.getToken();
        const canvas = this;
        const host = this.host;
        const contentType = (typeof data === 'string' || data instanceof String) ? 'application/json' : false;
        const video = this;
        const options = {
            contentType: contentType,
            host: host + '/graphql/secret',
            data: data,
            uploadCallback: (progress: number) => {
                this.setMessage('Uploading');
                this.setProgressBar(progress / 2);
            },
            callback: (response: any) => {
                let uploadData = (response.data) ? response.data : response;
                const dataErrors = (response.errors) ? response.errors : '';
                uploadData = (uploadData.upload) ? uploadData.upload : (uploadData.modify) ? uploadData.modify : '';
                console.log(uploadData);
                if (uploadData.resources) {
                    const id = uploadData.resources[0].id;
                    heartbeat(id, token, canvas);
                } else {
                    if (uploadData.errors || uploadData.error || dataErrors) {
                        const errors = uploadData.errors || uploadData.error || dataErrors[0].message;
                        console.log(errors);
                        const type = (errors) ? video.messageTypes.error : video.messageTypes.success;
                        const status = (uploadData.status) ? uploadData.status : '';
                        video.setMessage(status + errors, type);
                    } else {
                        video.setMessage('General error', video.messageTypes.error);
                    }
                }
            }
        };

        // this.editor.upload(options);
        // this.hideLoading();
        this.addSource(URL.createObjectURL(this.file));
    }

    // Extract option data to a string //
    getOption (option: Option, id: number): string {
        const output = (option.getGraphQLParam) ? option.getGraphQLParam(id) : '';

        return output;
    }

    getUploadQuery (): FormData {
        const formData: FormData = new FormData();
        formData.append('0', this.file);

        formData.append('map', '{"0": ["variables.file0"]}');

        const mutations = `mutation($file0: Upload)`;
        const transformations = '{ upload( job:[{video: $file0}])';
        const variables = ', "variables": {' + `"file0":[null]` + '}';
        const query = `${mutations} ${transformations} { auth{status,token}, resources{id} }`;
        const operations = '{ "query": "' + query + '}"' + variables + '}';
        formData.append('operations', operations);

        return formData;
    }

    getModifyQuery (): string {
        const canvas = this;
        const link = $(this.element).find('source').attr('src');
        let transformations = '{ modify( link:\\"' + link + '\\"';
        let options = '';
        const getOption = this.getOption;

        // remove source to clear out video data //
        $(canvas.element).find('source').remove();
        canvas.element.load();

        // build/convert options as graphQL query //
        if (canvas.options) {
            const retrievedOptions = [];
            for (const option of Object.keys(canvas.options)) {
                const value = getOption(canvas.options[option], canvas.id);

                if (value) {
                  retrievedOptions.push(value);
                }
            }
            if (retrievedOptions) {
              options = 'options: {' + retrievedOptions.join(',') + '}';
            }
        }
        if (options) {
            transformations += ',';
            transformations += options;
        }
        transformations += ''; // end options object

        return '{"query": "mutation ' + transformations + ') { auth{token,status}, resources{s3_path, id} } }"}';
    }

    getFormData (): any {
        if ($(this.element).find('source').length > 0) {
          return this.getModifyQuery();
        } else {
          return this.getUploadQuery();
        }
    }

    hasAudio (video: any): boolean {
        return video.mozHasAudio ||
            Boolean(video.webkitAudioDecodedByteCount) ||
            Boolean(video.audioTracks && video.audioTracks.length);
    }

    undo () {
      console.log('undo');
    }

    redo () {
      console.log('redo');
    }

    goBack () {
      console.log('back');
    }

    onClose (event: any) {
      console.log('on close');
    }
}


// https://opiateboard.drminc.com/?post_type=visualizer&#038;p=413