import { NgModule } from '@angular/core';
import {ItemComponent} from '../../mainmodal/canvascontainers/canvascontainer/item/item.component';
import {MatBadgeModule, MatIconModule} from '@angular/material';
import {VideoeditorComponent} from './videoeditor.component';

@NgModule({
  imports: [
    MatIconModule,
    MatBadgeModule
  ],
  declarations: [
    ItemComponent,
    VideoeditorComponent
  ],
  providers: [
  ],
  exports: [
    VideoeditorComponent
  ],
  entryComponents: [ ]
})
export class VideoeditorModule { }
