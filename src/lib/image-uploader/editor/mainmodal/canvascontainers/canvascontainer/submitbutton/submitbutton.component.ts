import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-submit-button',
    templateUrl: './submitbutton.component.html',
    styleUrls: ['./submitbutton.component.css']
})
export class SubmitbuttonComponent {

    public name = 'image_submit_button_';

    @Input() canvas: any; // used for getting options //
    @Input() type: any; // used to identify input type //
    @Input() button_title = 'Upload';

    constructor () {
    }

    onSubmit (e: MouseEvent): void {
        this.submit([this.canvas]);
    }

    // Extract option data to a string //
    // getOption (option : Option, id : number) : string {
    //     let output = (option.getGraphQLParam) ? option.getGraphQLParam(id) : '';
    //
    //     return output;
    // }

    // Perform ajax call to imagehost //
    // upload (formData) {
    //     let canvas = this.canvas;
    //     let host = this.type === 'video' ? this.editor.videoHost : this.editor.imageHost;
    //     $.ajax({
    //         type: "POST",
    //         url: host,
    //         data: formData,
    //         processData: false,
    //         contentType: false,
    //         xhr: function() {
    //             var xhr = $.ajaxSettings.xhr();
    //             xhr.upload.addEventListener("progress", function(evt) {
    //                 if (evt.lengthComputable) {
    //                     var percentComplete = 100 * (evt.loaded / evt.total);
    //                     //Do something with upload progress here
    //                     canvas.setProgressBar(percentComplete/2);
    //                 }
    //             }, false);
    //
    //             xhr.addEventListener("progress", function(evt) {
    //                 if (evt.lengthComputable) {
    //                     var percentComplete = evt.loaded / evt.total;
    //                     //Do something with download progress
    //                     canvas.setProgressBar(50 + percentComplete/2);
    //                 }
    //             }, false);
    //             return xhr;
    //         },
    //         headers: { 'Authorization': `Bearer ${this.editor.getCookie()}` },
    //         success: (data) => {
    //             console.log(data);
    //             let s3_path = data.data.url;
    //             this.heartbeat(s3_path);
    //             // canvas.setProgressBar(100);
    //             // this.editor.callback(data);
    //             // this.canvas.hideLoading();
    //         },
    //         error: (xhr, status, error) => {
    //             this.canvas.addMessage((!error) ? status :  error);
    //         },
    //         dataType: 'json'
    //     });
    // }

    // Return uniform file key name //
    getFileKey (id: number): string {
        return `file${id}`;
    }

    // Construct GraphQL query; then submit via Ajax //
    submit ( canvases: Array<any> ): void {
        // let transformation = (this.type === 'video') ? 'job' : 'transformations';
        // // full query //
        // var query : string = '';
        // // Transformation query //
        // var transformations = '{ upload( ' + transformation + ':[';
        // // Files in mutation //
        // var mutations = 'mutation(';
        // // Files in Variables //
        // var variables : string = ', "variables": {';
        // // Files in Map (Maps actual file data to GraphQL variable in php) //
        // var map : string = '{';
        // // Forms array of options passed to Cloudinary //
        // var options : string = '';
        // // Actual file data //
        // var formData : FormData = new FormData();
        //
        // let type = this.type;
        //
        // // Don't clone all of 'this' //
        // var getOption = this.getOption;
        // var getFileKey = this.getFileKey;
        // canvases.forEach( (canvas, a) => {
        //     map += '"' + a + '": ["variables.file' + a + '"]';
        //     mutations += `$${getFileKey(a)}: Upload`;
        //     transformations+= `{${type}: $file${a}`;
        //     if (a !== canvases.length - 1) {
        //         map += ',';
        //         mutations += ','
        //     }
        //
        //     formData.append(a, canvas.file);
        //
        //     if(canvas.options) {
        //         let retrievedOptions = [];
        //         for (let option in canvas.options) {
        //             let value = getOption(canvas.options[option], canvas.id);
        //
        //             if(value)
        //                 retrievedOptions.push(value);
        //         }
        //         if(retrievedOptions)
        //             options = 'options: {' + retrievedOptions.join(',') + '}';
        //     }
        //     if(options) {
        //         transformations += ',';
        //         transformations+=options;
        //     }
        //
        //     transformations += '}'; // end options object
        //
        //     // Variable data gets appended later via PHP //
        //     variables += `"${getFileKey(a)}":[null]`;
        //     if (a !== canvases.length - 1) {
        //         transformations += ',';
        //         variables += ',';
        //     }
        // });
        // transformations+='])';
        // mutations+=')';
        //
        // // Minimal returned options, see GraphQL Docs to see possible returned values //
        // query+=`${mutations} ${transformations} { ${this.editor.graphQLResponse} }`;
        // variables+= '}';
        // map+= '}';
        //
        // let operations = '{ "query": "' + query + '}"' + variables + '}';
        //
        // formData.append('operations', operations);
        // formData.append('map', map);
        //
        // let host = this.type === 'video' ? this.editor.videoHost : this.editor.imageHost;
        this.canvas.upload();
    }
}
