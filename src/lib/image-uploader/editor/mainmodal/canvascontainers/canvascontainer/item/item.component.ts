import {Component, Input} from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-item-loading',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.css']
})
export class ItemComponent {
  loadingSelector = 'app-item-loading';
  messageTypes = {
    'success': 'success',
    'error': 'error',
    'info': 'info'
  };
  public host: string;
  // Set element on ngOnInit //
  element: any;

  // File taken from file input //
  @Input() file: File;
  // Options within the option container //
  @Input() options: any; // Using OptionContainerComponent triggers cyclical dependency warnings
  // constructor() {}

  // Show a modal that is on top of everything is this container //
  showLoading(): void {
    const parent = $(this.element).closest('.canvas-container');
    const itemModal = $(this.element).siblings(this.loadingSelector).find('.item-loading-modal');

    itemModal.width(parent.width()).height(parent.height());
    itemModal.show();
  }

  // Hide a modal and clear progress bar that is on top of everything is this container //
  hideLoading(): void {
    const itemModal = $(this.element).siblings(this.loadingSelector).find('.item-loading-modal');
    $(this.element).siblings(this.loadingSelector).find('.item-loading-message').html('');
    $(this.element).siblings(this.loadingSelector).find('.item-loading-progress-bar').css('width', '0%');
    itemModal.hide();
  }

  // Set the percentage of progress bar which is a css filled div //
  setProgressBar(percentage: number): void {
    const progressBar = $(this.element).siblings(this.loadingSelector).find('.item-loading-progress-bar');
    progressBar.width(percentage + '%');
  }

  // Add message that appears above the progress bar //
  addMessage(message: string, type: string = 'success'): void {
    const messages = $(this.element).siblings(this.loadingSelector).find('.item-loading-message');
    messages.append(`<div class='loading-message-${type}'>${message}</div>`);
  }

  // set message that appears above the progress bar //
  setMessage(message: string, type: string = 'success'): void {
    const messages = $(this.element).siblings(this.loadingSelector).find('.item-loading-message');
    messages.html(`<div class='loading-message-${type}'>${message}</div>`);
  }
}
