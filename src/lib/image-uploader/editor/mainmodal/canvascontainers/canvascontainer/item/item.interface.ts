export interface ItemInterface {
    onResize(e : EventListener) : void;
    getElementScale() : {width : number, height : number};
    setElementSize(width : number, height : number) : HTMLVideoElement | HTMLCanvasElement;
    upload () : void;
}
