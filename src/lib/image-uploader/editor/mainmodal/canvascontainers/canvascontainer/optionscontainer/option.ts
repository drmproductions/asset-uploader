export abstract class Option {
    type: string;
    name: string;

    abstract getId(position : number): string;

    abstract getValue(position : number): any;

    abstract getGraphQLParam(id : number): string;

    abstract reset() : void;
}