import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FacebookService} from './assetselector/facebook/facebook.service';
import {SelectorModule} from './assetselector/selector.module';
import {MainmodalComponent} from '../mainmodal/mainmodal.component';
import {ImageeditorModule} from './imageeditor/imageeditor.module';
import {VideoeditorModule} from './videoeditor/videoeditor.module';

@NgModule({
    imports: [
      CommonModule,
      SelectorModule,
      ImageeditorModule,
      VideoeditorModule,
    ],
    declarations: [
      MainmodalComponent
    ],
    exports: [ MainmodalComponent ],
    entryComponents: [],
    providers: [ FacebookService ]
})
export class MainmodalModule {
    constructor () {}
}
