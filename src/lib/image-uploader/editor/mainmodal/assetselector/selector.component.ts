import {
    AfterViewChecked,
    ChangeDetectorRef,
    Component, ViewEncapsulation, Input, OnInit
} from '@angular/core';
import {EditorService} from '../../editor.service';
import {PulldownService} from '../../../pulldowns/pulldown.service';

@Component({
    selector: 'app-asset-selector',
    templateUrl: './selector.component.html',
    styleUrls: ['./selector.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class SelectorComponent implements AfterViewChecked, OnInit {

    assetOption: string;

    @Input() editor: EditorService;
    @Input() pulldown: PulldownService;

    constructor(private ref: ChangeDetectorRef) {
        console.log('selector constructor', this.editor);
    }

    ngOnInit(): void {
        console.log('selector init', this.editor);
    }

    ngAfterViewChecked(): void {
        this.ref.detectChanges();
    }

    getLocalFile (event: any): void {
        const file = event.target.files[0];
        if (file) {
            this.editor.addAsset(file);
        }
    }

    setOption (option: string): void {
      console.log('changing option', option);
        this.assetOption = option;
    }

    close (): void {
      console.log('selector close', this.pulldown);
        this.editor.close(this.pulldown.getRef(this.pulldown.ids.MAIN), this.pulldown.ids.MAIN);
    }

    showLibrary (): boolean {
        return (!this.editor.in_library);
    }
}
