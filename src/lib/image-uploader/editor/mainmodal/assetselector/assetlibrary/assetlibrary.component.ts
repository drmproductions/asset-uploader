import {
  Component, ChangeDetectorRef, Input, OnDestroy
} from '@angular/core';
import {CookieService} from 'ngx-cookie';
import {EditorService} from '../../../editor.service';
import {MedialibraryService} from '../../../../../medialibrary/medialibrary.service';
import {DirectoryService} from '../../../../../medialibrary/directory.service';
import {Resource} from '../../../../../interfaces/resource';
import {SortService} from '../../../../../medialibrary/sort.service';
import { MatDialog } from '@angular/material';
import {MedialibraryComponent} from '../../../../../medialibrary/medialibrary.component';
import {PulldownService} from "../../../../pulldowns/pulldown.service";

export interface CallbackResponse {
  resources: Array<Resource>;
}

@Component({
  selector: 'lib-asset-library',
  templateUrl: './assetlibrary.component.html',
  styleUrls: ['./assetlibrary.component.scss']
})
export class AssetlibraryComponent extends MedialibraryComponent {
  uploadCallback: Function;

  @Input() editor: EditorService;

  constructor(public mediaLibrarySerive: MedialibraryService, public cookieManager: CookieService,
              public directoryService: DirectoryService, public changeDetector: ChangeDetectorRef,
              public dialog: MatDialog, public pulldown: PulldownService) {
    // Constructor //
    super(mediaLibrarySerive, cookieManager, directoryService, changeDetector, dialog, pulldown);

    this.uploadCallback = function (add: CallbackResponse, assetSource: string) {
      console.log('add', add);
      console.log('assetSource', assetSource);
    };
  }
}
