import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Angulartics2Module } from 'angulartics2';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatSidenavModule, MatDialogModule
} from '@angular/material';
import {CookieModule, CookieService} from 'ngx-cookie';
import {HttpLink} from 'apollo-angular-link-http';
import {AssetlibraryComponent} from "./assetlibrary.component";
import {PulldownComponent} from "../../../../pulldowns/pulldown/pulldown.component";
import {FilepulldownComponent} from "../../../../pulldowns/filepulldown/filepulldowncomponent";

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    Angulartics2Module,
    FontAwesomeModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    CookieModule.forChild()
  ],
  declarations: [
    AssetlibraryComponent, FilepulldownComponent,
    PulldownComponent
  ],
  providers: [
    CookieService, HttpLink
  ],
  exports: [ AssetlibraryComponent ],
  entryComponents: [ AssetlibraryComponent, FilepulldownComponent ]
})
export class AssetlibraryModule { }
