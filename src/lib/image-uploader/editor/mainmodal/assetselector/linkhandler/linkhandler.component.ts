import {Component, ViewEncapsulation} from '@angular/core';
import {EditorService} from '../../../editor.service';

@Component({
    selector: 'app-link-handler',
    templateUrl: './linkhandler.component.html',
    styleUrls: ['./linkhandler.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class LinkhandlerComponent {

    displayLink = '';
    badImage = true;

    constructor(private editor: EditorService) {
    }

    onKey (event: any) {
        this.testImage(event.target.value).then( (result) => {
            if (result) {
                this.displayLink = event.target.value;
            } else {
                this.displayLink = '';
            }
        }).catch( (e) => {
            this.displayLink = '';
        });
    }

    setImage () {
      console.log('setting asset via link');
        if (this.displayLink) {
          this.editor.addAsset(this.displayLink);
        }
    }

    testImage(url: string, timeoutT?: number): Promise<boolean> {
        return new Promise(function (resolve: any, reject: any) {
            const timeout = timeoutT || 5000;
            let timer: any;
            const img = new Image();
            img.onerror = img.onabort = function () {
                clearTimeout(timer);
                reject(false);
            };
            img.onload = function () {
                clearTimeout(timer);
                resolve(true);
            };
            timer = setTimeout(function () {
                // reset .src to invalid URL so it stops previous
                // loading, but doesn't trigger new load
                img.src = '//!!!!/test.jpg';
                reject(false);
            }, timeout);
            img.src = url;
        });
    }
}
