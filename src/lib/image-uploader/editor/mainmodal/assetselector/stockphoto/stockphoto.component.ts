import {Component, Input, ViewEncapsulation, HostListener, OnInit} from '@angular/core';
import {EditorService} from '../../../editor.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {debounceTime, map, distinctUntilChanged} from 'rxjs/internal/operators';
import {Subject, Observable, Subscription, fromEvent} from 'rxjs';
import {ScrollingService} from '../scrolling.service';

// Unsplashed Result //
interface Result {
    results: Array<Image>;
    total_pages: number;
    total: number;
}
// Unsplashed Result image objext //
interface Image {
    urls: {
      regular: string;
      thumb: string;
    };
}

interface ResponseLoader {
    loading: boolean;
}

@Component({
    selector: 'app-stock-photo',
    templateUrl: './stockphoto.component.html',
    styleUrls: ['./stockphoto.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class StockphotoComponent implements OnInit {
  displayLink = ''; // link to send to image editor
  total: number; // total images found from service
  pages: number; // Total pages found from service
  page = 1; // The selected page
  perPage = 25; // Default number of images to pull per page
  images: Array<Image>; // The returned images
  loaded = 0; // How many images are loaded //
  loading = true; // If all images are loaded show images //
  @Input() query: string; // Search bar input //
  responseLoader: ResponseLoader; // Show loader only when a total is unknown (when new search term ) //
  keyUp: Subject<KeyboardEvent> = new Subject();

  constructor(private editor: EditorService, private http: HttpClient, private scollService: ScrollingService) {
    console.log('starting shutterfly');
    // Add debounce to search input //
    this.keyUp
      .pipe(
        map((event: any) => event.target.value),
        debounceTime(1000),
        distinctUntilChanged(),
      ).subscribe(query => {
      console.log('unsplash query', query);
      // Clear out vars to hide pages/total on frontend //
      this.query = query;
      this.page = 1;
      this.total = 0;
      this.pages = 0;
      this.loaded = 0;
      this.searchImage();
    });
    console.log('setup sub');
  }

  ngOnInit() {
    // Add scrolling tracker for overflow element wrapper, but use child to determine height //
    document.querySelector('.wrapper').addEventListener('scroll', (e) => {
      if (this.page < this.pages) {
        this.scollService.infiniteScrolling(e.target as HTMLElement, document.querySelector('.asset-option-content'),
          this.loading)
          .then( () => {
            this.page++;
            this.search();
          });
      }
    });
  }

  // Send XHR request to Unsplash with a query to find stock images //
  searchImage(page: number = this.page): void {
    console.log('searching image');
    this.images = [];
    this.responseLoader = {loading: true};
    this.search();
  }

  search (page: number = this.page) {
    console.log('searching image');
    if (this.editor.unsplashKey) {
      this.loading = true;
      this.http.get('https://api.unsplash.com/search/photos/?client_id=' + this.editor.unsplashKey,
        {params: new HttpParams().set('query', this.query).set('page', page + '').set('per_page', this.perPage + '')})
        .pipe(debounceTime(1000))
        .subscribe((result: Result) => {
          if (result) {
            this.images = this.images.concat(result.results);
            this.pages = result.total_pages;
            this.total = result.total;
            this.responseLoader.loading = false;
          }
        });
    } else {
      console.log('No unsplash key provided');
    }
  }

  // Choose an image to send to image editor, closes chooser //
  setImage(): void {
    if (this.displayLink) {
      this.editor.addAsset(this.displayLink);
    }
  }

  // Add css class to image when clicked on //
  selectImage(image: Image): void {
    this.displayLink = image.urls.regular;
  }

  // Handle loaded images to only show when all expected are loaded //
  load() {
    this.loaded++;
    console.log(this.loaded, this.images.length);
    if (this.loaded === this.images.length) {
      this.loading = false;
    }
  }
}
