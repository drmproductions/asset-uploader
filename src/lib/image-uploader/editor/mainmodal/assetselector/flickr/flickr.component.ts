import {Component, HostListener, OnInit, ViewEncapsulation} from '@angular/core';
import {EditorService} from '../../../editor.service';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ScrollingService} from '../scrolling.service';

interface Album {
    id: string;
    primary_photo_extras: {
        url_q: string
    };
    title: {
        _content: string
    };
}

interface FlickrResult {
    authUrl: string;
    albums: Array<Album>;
}

interface FlickrPhotosResult {
  photo: Array<FlickrPhoto>;
  pages: number;
  total: number;
}

interface FlickrPhoto {
  url_q: string;
  url_o: string;
}

@Component({
    selector: 'app-asset-flickr',
    templateUrl: './flickr.component.html',
    styleUrls: ['./flickr.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class FlickrComponent implements OnInit {

  displayLink = ''; // link to send to image editor
  flickrLink: string;
  loading = true;
  album: string; // Selected album
  albums: Array<Album>;
  photos: Array<any>;
  page = 1; // page to grab from album
  pages = 0; // total pages in album
  total = 0; // total images from all pages

  constructor(private editor: EditorService, private http: HttpClient, private scollService: ScrollingService) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.editor.getToken()}`
      })
    };
    const link = this.editor.imageHost + 'flickr';
    this.http.get(link, httpOptions).subscribe((result: FlickrResult) => {
      this.loading = false;
      if (result) {
        if (result.authUrl) {
          this.flickrLink = result.authUrl;
        } else if (result.albums) {
          this.albums = result.albums;
        }
      }
    });
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: any): void {
    console.log('welcome');
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: any): void {
    console.log('bye');
  }

  ngOnInit() {
    // Add scrolling tracker for overflow element wrapper, but use child to determine height //
    document.querySelector('.wrapper').addEventListener('scroll', (e) => {
      if (this.page < this.pages) {
        this.scollService.infiniteScrolling(e.target as HTMLElement, document.querySelector('.asset-option-content'),
          this.loading)
          .then(() => {
            this.page++;
            this.getImages(this.page);
          });
      }
    });
  }

  chooseAlbum(album: string) {
    this.loading = true;
    this.photos = null;
    this.page = 1;
    this.pages = 0;
    this.album = album;

    this.getImages(this.page);
  }

  getImages(page: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.editor.getToken()}`
      }),
      params: new HttpParams().set('album', this.album).set('page', page + '')
    };
    const link = this.editor.imageHost + '/flickr/photos';
    this.http.get(link, httpOptions).subscribe((result: FlickrPhotosResult) => {
      this.loading = false;
      if (result && Array.isArray(result.photo)) {
        this.photos = result.photo;
        this.pages = result.pages;
        this.total = result.total;
      } else {
        console.log('No results or photo is not an array');
      }
    });
  }

  goBackToAlbums() {
    this.photos = null;
    this.album = null;
  }

  hasPhotos(): boolean {
    if (this.photos) {
      return true;
    } else {
      return false;
    }
  }

  // Choose an image to send to image editor, closes chooser //
  setImage(): void {
    if (this.displayLink) {
      this.editor.addAsset(this.displayLink);
    }
  }

  // Add css class to image when clicked on //
  selectImage(image: string): void {
    this.displayLink = image;
  }

  // Handle loaded images to only show when all expected are loaded //
  load() {
    // this.loaded ++;
    // if (this.loaded === this.expectedImages) {
    //     this.loading = false;
    // }
  }
}
