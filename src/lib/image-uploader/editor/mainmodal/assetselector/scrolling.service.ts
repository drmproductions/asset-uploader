import {Injectable} from '@angular/core';

@Injectable()
export class ScrollingService {

  infiniteScrolling(parent: HTMLElement, element: HTMLElement, loading = false, tolerance = 75): Promise<void> {
    return new Promise ( (resolve, reject) => {
        let pctDownPage = this.getScrollPct(parent, element);

        if (pctDownPage >= tolerance && !loading) {
          return resolve();
        }
    });
  }
  // get percent of dom scrolled //
  getScrollPct(parent: HTMLElement, element: HTMLElement) {
    let scrollTop = parent.scrollTop;
    let parentHeight = parent.clientHeight;
    let height = element.clientHeight;
    let offset = element.offsetHeight;
    let totalHeight = height + parentHeight;

    var pctScrolled = Math.floor(scrollTop / totalHeight * 100);

    return Math.abs(pctScrolled);
  }
}