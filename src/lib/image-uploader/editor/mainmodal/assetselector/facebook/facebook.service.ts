import { Injectable } from '@angular/core';
import { InitParams } from './models/init-params';
import { LoginStatus } from './models/login-status';
import {Observable} from 'rxjs';

declare var FB: any;

/**
 * @hidden
 */
export type ApiMethod = 'get' | 'post' | 'delete';

/**
 * @shortdesc
 * Angular 2 service to inject to use Facebook's SDK
 * @description
 * You only need to inject this service in your application if you aren't using [`FacebookModule`](../facebook-module).
 * @usage
 * ```typescript
 * import { FacebookService, InitParams } from 'ng2-facebook-sdk';
 *
 * constructor(private fb: FacebookService) {
 *
 *   const params: InitParams = {
 *
 *   };
 *
 *   fb.init(params);
 *
 * }
 * ```
 */
@Injectable()
export class FacebookService {

    init(params: InitParams): Promise<any> {
        try {
            return Promise.resolve(FB.init(params));
        } catch (e) {
            return Promise.reject(e);
        }
    }

    api(path: string, method: ApiMethod = 'get', params: any = {}): Promise<any> {
        return new Promise<any>((resolve, reject) => {

            try {
                FB.api(path, method, params, (response: any) => {
                    if (!response) {
                        reject();
                    } else if (response.error) {
                        reject(response.error);
                    } else {
                        resolve(response);
                    }
                });
            } catch (e) {
                reject(e);
            }

        });
    }

    getLoginStatus(forceFreshResponse?: boolean): Promise<LoginStatus> {
        return new Promise<LoginStatus>((resolve, reject) => {

            try {
                FB.getLoginStatus((response: LoginStatus) => {
                    if (!response) {
                        reject();
                    } else {
                        resolve(response);
                    }
                }, forceFreshResponse);
            } catch (e) {
                reject(e);
            }

        });
    }

    logout(): Promise<any> {
        return new Promise<any>((resolve, reject) => {

            try {
                FB.logout((response: any) => {
                    resolve(response);
                });
            } catch (e) {
                reject(e);
            }

        });
    }

    watchStatus (): Observable<any> {
        return new Observable(observer => {
            try {
                FB.Event.subscribe('auth.statusChange', (response: any) => {
                    observer.next(response);
                });
            } catch (e) {
                console.log(e);
            }
        });
    }

    watchLogin (): Observable<any> {
        return new Observable((observer: any) => {
            try {
                FB.Event.subscribe('auth.login', (response: any) => {
                    observer.next(response);
                });
            } catch (e) {
                console.log(e);
            }
        });
    }
}
