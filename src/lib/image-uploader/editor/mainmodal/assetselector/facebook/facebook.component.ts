import {
    ChangeDetectorRef,
    Component, ViewEncapsulation
} from '@angular/core';
import {EditorService} from '../../../editor.service';
import {FacebookService} from './facebook.service';
import {ScriptLoaderService} from '../../../scriptLoaderService';

declare var FB: any;

@Component({
    selector: 'app-facebook-selector',
    templateUrl: './facebook.component.html',
    styleUrls: ['./facebook.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class FacebookComponent {

    responseLoading = true;
    imagesLoading = false;
    is_logged_in = false;
    images: Array<any>;
    imagesLoaded = 0;
    displayLink: string;

    constructor(private editor: EditorService, private fb: FacebookService, private loader: ScriptLoaderService,
                private ref: ChangeDetectorRef) {
        this.loader.load({src: 'https://connect.facebook.net/en_US/sdk.js', name: 'fb'})
            .pipe()
            .subscribe(() => {
                fb.init({
                    appId: editor.fbAppId,
                    autoLogAppEvents: true,
                    xfbml: true,
                    version: 'v3.2',
                    status: true
                });

                this.fb.watchStatus().subscribe( (response) => {
                    console.log(response);
                    if (response.status === 'connected') {
                        this.is_logged_in = true;
                    }
                    this.responseLoading = false;
                    this.ref.detectChanges();
                    this.imagesLoading = true;
                    this.getImages().then( (images) => {
                        this.images = images;
                    });
                });

                this.fb.watchLogin().subscribe( (response) => {
                    console.log(response);
                });

                this.ref.detectChanges();
            });
    }

    // Add css class to image when clicked on //
    selectImage (image: string): void {
        this.displayLink = image;
    }
    // Choose an image to send to image editor, closes chooser //
    setImage (): void {
        if (this.displayLink) {
            this.editor.addAsset(this.displayLink);
        }
    }

    getImages (): Promise<Array<string>> {
        const images: any = [];
        let counter = 0;
        return new Promise((resolve: any, reject: any) => {
            FB.api('me/albums', (response: any) => {
                console.log(response);
                for (let i = 0; i < response.data.length; i++) {
                    const album = response.data[i];
                    FB.api('/' + album.id + '/photos', {fields: 'images.width(2048),album'},  (photos: any) => {
                        console.log('photos', photos);
                        counter++;
                        if (photos && photos.data && photos.data.length) {
                            photos.data.forEach( (datum: any) => {
                                images.push(datum.images[0].source);
                            });
                            console.log(counter, response.data.length);
                            if (counter === response.data.length) {
                                resolve(images);
                            }
                        }
                    });
                }
            });
        });
    }
    // Handle loaded images to only show when all expected are loaded //
    load () {
        this.imagesLoaded++;
        console.log(this.imagesLoaded, this.images.length);
        if (this.imagesLoaded === this.images.length) {
            this.imagesLoading = false;
        }
    }
}
