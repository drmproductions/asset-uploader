import { NgModule } from '@angular/core';
import {SelectorComponent} from './selector.component';
import {MatBadgeModule, MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {LinkhandlerComponent} from './linkhandler/linkhandler.component';
import {StockphotoComponent} from './stockphoto/stockphoto.component';
import {FacebookComponent} from './facebook/facebook.component';
import {FlickrComponent} from './flickr/flickr.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AssetlibraryModule} from './assetlibrary/assetlibrary.module';
import {ScrollingService} from './scrolling.service';

@NgModule({
  imports: [
    AssetlibraryModule,
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    FormsModule,
    MatIconModule,
    MatBadgeModule,
    BrowserAnimationsModule
  ],
  declarations: [
    LinkhandlerComponent,
    StockphotoComponent,
    FacebookComponent,
    FlickrComponent,
    SelectorComponent
  ],
  exports: [
    SelectorComponent
  ],
  providers: [ ScrollingService ]
})
export class SelectorModule { }
