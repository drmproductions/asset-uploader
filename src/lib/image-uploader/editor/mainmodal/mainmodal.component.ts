import {
    Component, Input, Output, EventEmitter, ViewEncapsulation, OnDestroy
} from '@angular/core';
import {EditorService} from '../editor.service';
import {PulldownService} from "../../pulldowns/pulldown.service";

// declare const $: any;
declare global {
    interface Window {
        File: File;
        FileList: FileList;
        FileReader: FileReader;
    }
}

@Component({
    selector: 'app-main-modal',
    templateUrl: './mainmodal.component.html',
    styleUrls: ['./mainmodal.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class MainmodalComponent implements OnDestroy {
  @Input() multiple: boolean;
  @Input() is_open = true;
  @Output() modalFlag = new EventEmitter<boolean>();

  editor: EditorService;
  pulldown: PulldownService;

  constructor() {
      document.body.style.overflow = 'hidden';
  }

  ngOnDestroy(): void {
      document.body.style.overflow = 'initial';
  }

    onClose (e: any) {
    this.editor.close(this.pulldown.getRef(this.pulldown.ids.MAIN), this.pulldown.ids.MAIN);
  }
}
