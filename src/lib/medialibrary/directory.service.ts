import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie';

import {SortState} from './medialibrary.component';
import {SortService} from '../medialibrary/sort.service';
import {AssetFile} from '../interfaces/asset.file';
import {Resource} from '../interfaces/resource';
import {MedialibraryService} from './medialibrary.service';

@Injectable()
export class DirectoryService {
  public assets: any = [];
  public files: Array<AssetFile> = [];
  sortStates: SortState = {
    folder: {
      name: 'folder',
      sort: (files: Array<AssetFile>) => {
        this.files = this.sortService.folderSort(files);
      }
    },
    asc: {
      name: 'arrow_downward',
      sort: (files: Array<AssetFile>) => {
        this.files = files.sort(this.sortService.ascNameSort);
      }
    },
    desc: {
      name: 'arrow_upward',
      sort: (files: Array<AssetFile>) => {
        this.files = files.sort(this.sortService.descNameSort);
      }
    }
  };

  constructor(private httpClient: HttpClient, private cookieManager: CookieService, private sortService: SortService,
              private media: MedialibraryService) {
  }

  moveFile (from: any, toPath: string, sortType: string, currentDirectory: string, organization: number, newFolder = false): boolean {
    if (!from || !toPath) {
      this.media.toggleMoveFlash('block');
      return false;
    }

    if (newFolder) {
      if ( toPath === '/') {
        toPath = currentDirectory.split('/').shift();
      } else {
        toPath = currentDirectory + '/' + toPath;
      }
    }
    console.log('from', from, 'to', toPath);
    const isToFolder = this.isFolder(toPath);
    if (!isToFolder || from.path === toPath) {
      console.log('to is not a folder');
      return false;
    }

    const isFromFolder = this.isFolder(from.path);
    if (!isFromFolder) {
      console.log('is from file');
      this.moveFileIntoFolder(from.path, toPath, sortType, currentDirectory, organization);
    } else {
      console.log('from is a folder');
      const path = from.type === 'folder' ? from.path : from.directory;
      this.moveFolderIntoFolder(path, toPath, sortType, currentDirectory, organization);
    }

    return true;
  }

  moveFileIntoFolder (urlPath: string, newFolder: string, sortType: string, currentDirectory: string, organization: number): boolean {
    for (let a = 0; a !== this.assets.length; a++) {
      if (urlPath === this.assets[a].path) {
        this.assets[a].directory = newFolder;
        this.media.graphMoveImage(urlPath, newFolder).subscribe( (result) => {
          console.log('file moved', result);
        });
        // this.assets[a].pending = true;
        this.sortFiles(sortType, currentDirectory, organization);
        return true;
      }
    }

    return false;
  }

  moveFolderIntoFolder (fromFolderPath: string, toFolderPath: string, sortType: string, currentDirectory: string, organization: number) {
    console.log('From', fromFolderPath, 'To', toFolderPath);
    // get all files from dragged folder //
    for (let a = 0; a !== this.assets.length; a++) {
      if ( this.isInDirectory(fromFolderPath, this.assets[a], true) ) {
        console.log(' ');
        console.log('------------------------------');
        console.log('found match', fromFolderPath, toFolderPath, this.assets[a].name);
        const newFolderPath = this.getNewPath(fromFolderPath, toFolderPath);
        // this.assets[a].path = this.movedFilePath(this.assets[a].path, newFolderPath);
        console.log(newFolderPath);
        console.log('------------------------------');
        console.log(' ');
        this.assets[a].directory = newFolderPath;
        // this.assets[a].pending = true;
        this.media.graphMoveImage(this.assets[a].path, newFolderPath).subscribe( (result) => {
          console.log('file moved', result);
        });
      }
    }

    this.sortFiles(sortType, currentDirectory, organization);
  }

  getNewPath (fromPath: string, toPath: string): string {
    let newPath = '';
    if (fromPath.indexOf(toPath) !== -1) {
      console.log('To dest is in path, moving up');
      const thisDir = fromPath.split('/').pop();
      newPath = toPath + '/' + thisDir;
    } else {
      const restOfPath = this.getDirDifference(fromPath, toPath);
      newPath = toPath + restOfPath;
    }

    return newPath;
  }

  isInDirectory (directoryToCheck: string = '', fileToCheck: any, deep?: boolean) {
    if (!directoryToCheck || !fileToCheck) {
      return false;
    }

    const fileDirectory = fileToCheck.directory + '/';
    const toDirectory = directoryToCheck + '/';

    if ( deep ) {
      return fileDirectory.indexOf(toDirectory) !== -1;
    }

    if (fileToCheck.directory === directoryToCheck) {
      return true;
    }
  }

  isParentDir (directory: string, comparison: string): boolean {
    const path = directory.split('/').slice(0, -1).join('/');
    return comparison === path;
  }

  findMatch (startingDirectory: string = '', url: any, deep?: boolean) {
    if (!startingDirectory || !url) {
      return false;
    }
    if (this.isFolder(url.path)) {
      return url.path.indexOf(startingDirectory) !== -1;
    } else {
      if (deep) {
        return url.directory.indexOf(startingDirectory) !== -1;
      }
      return  startingDirectory === url.directory;
    }
  }

  // Get difference of fromDir to ToDir //
  getDirDifference (fromPath: string, toPath: string) {
    const from = fromPath.split('/');
    const to = toPath.split('/');

    for (let a = from.length - 1; a !== -1; a--) {
      for (let b = to.length - 1; b !== -1; b--) {
        if (from[a] === to[b]) {
          from.splice(b, 1);
        }
      }
    }

    let difference = from.join('/');
    difference = difference ? '/' + difference : '';
    return difference;
  }

  movedFilePath (filePath: string, newPath: string, organization: number) {
    const fileName = filePath.split('/').pop();
    const newFilePath = filePath.split('/' + organization + '/').shift() + '/' +
      newPath ;
    console.log(newFilePath);
    return newFilePath;
  }

  // Called from button on sidenav menu to rename a file or all files in a folder to new name //
  renameFile (filePath: string, newName: string, sortType: string, currentDirectory: string, organization: number): string {
    if (this.isFolder(filePath)) {
      return this.renameFolder(filePath, newName);
    } else {
      return this.renameSingleFile(filePath, newName);
    }

    this.sortFiles(sortType, currentDirectory, organization);
  }

  renameSingleFile (filePath: string, newName: string): string {
    for (let a = 0; a !== this.assets.length; a++) {
      if (filePath === this.assets[a].path) {
        const ext = this.assets[a].path.split('.').pop();
        newName += '.' + ext;
        // this.assets[a].path = this.getURLDirectory(this.assets[a]) + '/' + newName;
        // this.assets[a].pending = true;
        this.media.graphRenameImage(this.assets[a].path, newName).subscribe();
        this.assets[a].name = newName;
        console.log('rename', this.assets);
        return newName;
      }
    }

    return '';
  }

  renameFolder (folderPath: string, newFolderName: string): string {
    for (let a = 0; a !== this.assets.length; a++) {
      if (this.isInDirectory(folderPath, this.assets[a], true)) {
        // Take last slah path and append it to new folder's path //
        console.log(folderPath);
        console.log('rename full path', this.assets[a].path);
        // const restOfOldPath = this.getDirectoryFromURL(this.assets[a].path).split(folderPath).pop();
        const restOfOldPath = this.assets[a].directory.split(folderPath).pop();
        console.log('rest of old path', restOfOldPath);
        const newFolderPath = folderPath.split('/').slice(0, -1).join('/') + '/' + newFolderName + restOfOldPath;
        console.log('new path', newFolderPath);
        // this.assets[a].path = this.movedFilePath(this.assets[a].path, newFolderPath);
        this.media.graphMoveImage(this.assets[a].path, newFolderPath).subscribe();
        this.assets[a].directory = newFolderPath;
        // this.assets[a].pending = true;
      }
    }

    return newFolderName;
  }

  deleteAsset (file: AssetFile, sortType: string, currentDirectory: string, organization: number) {
    if (!this.isFolder(file.path)) {
      const assetIndex = this.findAssetIndex(file.path);
      console.log('is not a folder', assetIndex);
      if (assetIndex !== null) {
        this.media.graphdecrementImage(file.path).subscribe( (result) => {} );
        this.assets.splice(assetIndex, 1);
        this.sortFiles(sortType, currentDirectory, organization);
        return true;
      }
    } else { // Delete all files in folder as well //
      console.log('deleting folder', file);
      let is_update = false;
      for (let a = this.assets.length - 1; a !== -1; a--) {
        if ( this.isInDirectory(file.path, this.assets[a], true) ) {
          console.log('deleteing', this.assets[a]);
          this.media.graphdecrementImage(this.assets[a].path).subscribe( (result) => {} );
          is_update = true;
          this.assets.splice(a, 1);
        }
      }
      if (is_update) {
        this.sortFiles(sortType, currentDirectory, organization);
        return true;
      }
    }
  }

  findAssetIndex (url: string): number {
    for (let a = 0; a !== this.assets.length; a++) {
      if (this.assets[a].path === url) {
        return a;
      }
    }

    return null;
  }

  getAllDirectories (originFile: any = null): Array<string> {
    const found: any = {};
    // loop through all files to find what is in selected directory //
    for (let i = 0; i !== this.assets.length; i++) {
      const path = this.getURLDirectory(this.assets[i]);
      const fullPath = path + '/';
      const originPath = (originFile) ? originFile.path : '';
      // if not parent and not sub //
      if (this.getDirDifference(originPath, path) && !this.isParentDir(originPath, path)) {
        found[path] = path;
      }
    }
    // convert object to array //
    const foundArray = [];
    for (const file of Object.keys(found)) {
      const folder = found[file];
      foundArray.push(folder);
    }

    return foundArray;
  }

  getURLDirectory (file: any): string {
    if (file.type === 'folder') {
      return null;
    }

    return file.directory;
  }

  getResourceURL (file: any): string {
    const url: string = (file.path) ? file.path : (file.url) ? file.url : file.s3_path;

    return url;
  }

  isFolder (path: string): boolean {
    // If file path contains a dot in last / path, it is not a folder //
    return path.split('/').pop().indexOf('.') === -1;
  }

  getDirectoryFromURL (url: string, organization: number): string {
    const dir = url.split('/' + organization + '/').pop().split('/').slice(0, -1).join('/');
    const directory = (dir) ? organization + '/' + dir : organization + '';

    return directory;
  }

  // Get folders in currently selected directory //
  getAllFromDirectory(currentDirectory: string, organization: number): Array<File> {
    const found: any = {};
    // loop through all files to find what is in selected directory //
    for (let i = 0; i !== this.assets.length; i++) {
      const url = this.getResourceURL(this.assets[i]);
      // get the next folder name or file name in asset url //
      console.log(this.assets[i]);
      const inPath = this.getNext(currentDirectory, this.assets[i].directory, organization);
      // keep as an object to avaoid adding duplicate folders (i.e test/a/test.png, test/a/test2.png) only want "a" once //
      if (inPath) {
        found[inPath.name] = inPath;
      }
    }
    // convert object to array //
    const foundArray = [];
    for (const file of Object.keys(found)) {
      const folder = found[file];
      folder.type = 'folder';
      folder.pending = false;
      foundArray.push(folder);
    }

    return foundArray;
  }

  getNext(currentDirectory: string, fullPath: string, organization: number) {
    if (!fullPath) {
      console.log('no fullpath');
      return null;
    }
    fullPath = '/' + fullPath;

    // currentDirectory = currentDirectory.replace(/\//g, '\\/');
    // "http://res.cloudinary.com/dye3phxtd/image/upload/v1541609517/23/test/test/wfnzpqwlr4plxmu0nwec.jpg"
    // .match(/\/23\/test\/([^\\\/:*?\"<>|]+)/);
    const b: any = '\\/' + currentDirectory + '\\/([^\\\\\\/:*?\\"<>|]+)';
    const re = new RegExp(b);
    if (fullPath.match(re)) {
      const afterMatch = fullPath.match(re)[1];
      console.log('has match', fullPath, currentDirectory, afterMatch);
      if (afterMatch.indexOf('.') === -1) {
        console.log('after match has no dot');
        console.log('full path', fullPath);
        console.log('aftermatch', afterMatch);
        const directoryPath = fullPath.split('/' + organization + '/').pop().split('/').slice(0, -1).join('/');
        console.log('dir path', directoryPath);
        // const path = (directoryPath) ? this.authService.user.org_logged_as + '/' + directoryPath : this.authService.user.org_logged_as;
        const path = currentDirectory + '/' + afterMatch;
        console.log('path', path);
        return {
          name: afterMatch,
          path: path
        };
      }
    }

    return null;
  }

  sortFiles (sortType: string, currentDirectory: string, organization: number): void {
    console.log('sort type', sortType);
    console.log('sort getting all from directory', currentDirectory);
    const files = this.assets.concat(this.getAllFromDirectory(currentDirectory, organization));
    this.sortStates[sortType].sort(files);
  }

  // Get the filename from Cloudinary url //
  getURLFilename (file: Resource | AssetFile): string {
    const url = this.getResourceURL(file);

    if (!url) {
      return '';
    }

    return url.split('/').pop();
  }

  getSortType (sortState: string): string {
    let index = 0;
    // find index current state is in //
    for (let a = 0; a !== Object.keys(this.sortStates).length; a++) {
      if (sortState === Object.keys(this.sortStates)[a]) {
        index = a;
        console.log('sort index', a);
        break;
      }
    }
    // Get next sortState or go to first state //
    if (index === Object.keys(this.sortStates).length - 1) {
      console.log('sort go to fisrt', Object.keys(this.sortStates)[0]);
      return Object.keys(this.sortStates)[0];
    } else {
      console.log('sort go to next', Object.keys(this.sortStates)[index + 1]);
      return Object.keys(this.sortStates)[index + 1];
    }
  }
}
