import {Injectable} from '@angular/core';
import {AssetFile} from '../interfaces/asset.file';

@Injectable()
export class SortService {

  constructor() {}

  folderSort (files: any) {
    const tempFolders = [];
    const tempRest = [];
    for (let a = 0; a !== files.length; a++) {
      if (files[a].type === 'folder') {
        tempFolders.push(files[a]);
      } else {
        tempRest.push(files[a]);
      }
    }
    tempFolders.sort(this.descNameSort);
    tempRest.sort(this.descNameSort);

    return tempFolders.concat(tempRest);
  }

  descNameSort(a: AssetFile, b: AssetFile) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  ascNameSort(a: AssetFile, b: AssetFile) {
    if (a.name > b.name) {
      return -1;
    }
    if (a.name < b.name) {
      return 1;
    }
    return 0;
  }
}
