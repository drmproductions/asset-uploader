import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Angulartics2Module } from 'angulartics2';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SortService} from '../medialibrary/sort.service';
import {ImageUploaderModule} from '../image-uploader/image-uploader.module';
import {MatFormFieldModule, MatIconModule, MatOptionModule, MatSelectModule, MatSidenavModule, MatInputModule} from '@angular/material';
import {CookieModule, CookieService} from 'ngx-cookie';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    Angulartics2Module,
    FontAwesomeModule,
    ImageUploaderModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    CookieModule.forChild()
  ],
  declarations: [
  ],
  providers: [
    SortService, CookieService
  ],
  exports: [ ],
  entryComponents: [ ]
})
export class MedialibraryModule { }
