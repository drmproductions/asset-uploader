import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie';

import gql from 'graphql-tag';
import {Apollo} from 'apollo-angular';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {setContext} from 'apollo-link-context';
import {HttpLink} from 'apollo-angular-link-http';
import {ApolloLink} from 'apollo-link';

@Injectable()
export class MedialibraryService {
  constructor(private httpClient: HttpClient, private cookieManager: CookieService, private apollo: Apollo,
              private httpLink: HttpLink) {
  }

  createClient(host: string, token: string, clientName: string) {
    // Setup link to image host api //
    const imageLink = this.httpLink.create({
      uri: host
    });
    if (!this.apollo.getClient()) {
      const context = this.getContext(token);
      this.apollo.create({
        link: context.concat(imageLink),
        cache: new InMemoryCache()
      }, clientName);
    }
    // Setup link to video host api //
    const videoLink = this.httpLink.create({
      uri: host
    });

    if (!this.apollo.getClient()) {
      const context = this.getContext(token);
      this.apollo.create({
        link: context.concat(videoLink),
        cache: new InMemoryCache()
      }, 'video');
    }
  }

  graphGetImages(keepCacheFlag: boolean): Observable<object> {
    const cache = keepCacheFlag ? 'cache-first' : 'no-cache';
    console.log('cache flag', keepCacheFlag, cache);
    return this.apollo.use('image')
      .watchQuery({
        query: gql`query {
             get {
              auth {
                status,
                token
              },
              resources {
                url,
                directory,
                name
              },
              status
            }
        }`,
        fetchPolicy: cache
      }).valueChanges;
  }

  graphMoveImage(url = '', directory = ''): Observable<object> {
    console.log('moving image', url, directory);
    return this.apollo.use('image')
      .mutate({
        mutation: gql`mutation moveMutation($url: String!, $directory: String!) {
            move(url: $url, directory: $directory) {
            auth {
              status,
              token
            },
            resources {
              url,
              directory
            },
            status
          }
        }`,
        variables: {
          url,
          directory
        }
      });
  }

  graphRenameImage(url = '', name = ''): Observable<object> {
    console.log('rename image', url, name);
    return this.apollo.use('image')
      .mutate({
        mutation: gql`mutation renameMutation($url: String!, $name: String!) {
            rename(url: $url, name: $name) {
            auth {
              status,
              token
            },
            resources {
              url,
              directory
            },
            status
          }
        }`,
        variables: {
          url,
          name
        }
      });
  }

  graphGetVideos(keepCacheFlag = true): Observable<object> {
    const cache = keepCacheFlag ? 'cache-first' : 'no-cache';
    return this.apollo.use('video')
      .watchQuery({
        query: gql`query {
            videos {
              auth {
                status,
                token
              },
              resources {
                s3_path,
                old_path,
                error,
                directory,
                id
              }
            }
        }`}).valueChanges;
  }

  graphdecrementImage(url: string): Observable<object> {
    console.log('deleting image', url);
    let from_library = true;
    return this.apollo.use('image')
      .mutate({
        mutation: gql`mutation decrementMutation($url: String!, $from_library: Boolean!) {
            decrement(url: $url, from_library: $from_library) {
            auth {
              status,
              token
            },
            deleted {
              files
            },
            status
          }
        }`,
        variables: {
          url,
          from_library
        }
      })
  }

  getContext (token: string): ApolloLink {
    return setContext(async (_, {headers}) => {
      // Grab token if there is one in storage or hasn't expired
      if (!token) {
        return {};
      }
      // Return the headers as usual
      return {
        headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
      };
    });
  }

  toggleMoveFlash (setTo: string): boolean {
    let moveFlash = document.querySelector('.move-failed-flash') as HTMLElement;
    console.log(setTo, moveFlash)

    if (setTo) {
      if (setTo === 'none') {
        moveFlash.style.display = 'none';
      } else {
        moveFlash.style.display = 'block';
      }

      return true;
    }

    if (moveFlash.style.display === 'none') {
      moveFlash.style.display = 'block';
    } else {
      moveFlash.style.display = 'none';
    }

    return true;
  }
}
