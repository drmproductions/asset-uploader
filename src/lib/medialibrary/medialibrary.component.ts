import {
  Component,
  Input,
  OnInit, ViewChild, ChangeDetectorRef
} from '@angular/core';
import {CookieService} from 'ngx-cookie';
import {MedialibraryService} from '../medialibrary/medialibrary.service';
import {SortService} from '../medialibrary/sort.service';
import { MatSidenav, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {DirectoryService} from '../medialibrary/directory.service';
import {EditorService} from '../image-uploader/editor/editor.service';
import {AssetFile} from '../interfaces/asset.file';
import {Resource} from '../interfaces/resource';
import {AddFolderModal} from '../modals/addFolder/addFolder.modal';
import { AngularFireAuth } from '@angular/fire/auth';
import {FinalStepPullDown} from "../image-uploader/pulldowns/finalstep/finalStep";
import {PulldownService} from "../image-uploader/pulldowns/pulldown.service";
import {FilepulldownComponent} from "../image-uploader/pulldowns/filepulldown/filepulldowncomponent";
import {PulldownComponent} from "../image-uploader/pulldowns/pulldown/pulldown.component";

export interface MediaLibraryResponse {
  data: {
    get: {
      auth: {
        status: number,
        token: string
      },
      folder: Folder,
      resources: Resource
    };
  };
}

export interface Folder {
  name: string;
  url: string;
}

export interface SortObject {
  name: string;
  sort: Function;
}
export interface SortState {
  folder: SortObject;
  asc: SortObject;
  desc: SortObject;
}

interface FileSideNav extends MatSidenav {
  file: AssetFile;
}

export interface User {
  first_name: string;
  last_name: string;
  phone_number: string;
  uid: string;
  email: string;
  refreshToken: string;
  firebase: AngularFireAuth;
  username: string;
  organization: Organization;
  org_logged_as: number;
}

export interface Organization {
  name: string;
  id: number;
  role: string;
  role_id: number;
}

export interface CallbackResponse {
  resources: Array<Resource>;
}

@Component({
  selector: 'app-media-library-module',
  templateUrl: './medialibrary.component.html',
  styleUrls: ['./medialibrary.component.scss']
})
export class MedialibraryComponent implements OnInit {
  @ViewChild('drawer') public drawer: FileSideNav;
  @Input() imageHost: string;
  @Input() videoHost: string;
  @Input() authHost: string;
  @Input() user: User;
  @Input() errorImage: string;
  @Input() editor: EditorService;

  currentDirectory: Array<string>;
  currentDepth = 0;
  term = '';
  timer: any;
  preventSimpleClick = false;
  selectedFile: AssetFile;
  sortState = this.directoryService.sortStates.folder.name;
  isLoading = true;
  uploadCallback: Function;
  isDrawerOpen = false;

  constructor(public mediaLibrarySerive: MedialibraryService, public cookieManager: CookieService,
              public directoryService: DirectoryService, public changeDetector: ChangeDetectorRef,
              public dialog: MatDialog, private _pulldown: PulldownService) {
  }

  ngOnInit() {
    console.log('media oninit');
    if (!this.imageHost) {
      this.imageHost = this.editor.imageHost;
    }
    if (!this.videoHost) {
      this.videoHost = this.editor.videoHost;
    }
    // this.uploadCallback = this.uploadCallback.bind(this);
    // this.mediaLibrarySerive.getAssets()
    //   .subscribe((assets: any) => {
    //     console.log(assets);
    //     this.assets = assets['data']['get'][0]['resources'];
    //     console.log(this.assets);
    //     this.folders = assets['data']['get'][0]['folders'];
    //     this.getFolderImages('all');
    //   });
    console.log('passed user', this.user);
    console.log('media library editor', this.editor);
    this.editor.authHost = this.authHost + 'api/v1/verify';
    if (this.user) {
      this.editor.user = this.user;
    }
    console.log('Media library init');
    if (!this.editor.user) {
      console.log('no user');
      this.editor.setUser().subscribe( (val: any) => {
        console.log('any val', val);
        this.setup();
      });
    } else {
      this.setup();
    }

    const that = this;
    this.uploadCallback = function (add: CallbackResponse, assetSource: string) {
      console.log('add', add);
      console.log('assetSource', assetSource);
      console.log('assets before', that.directoryService.assets);
      const images = that.convertFiles(add.resources);
      console.log('images', images);
      that.directoryService.assets = that.directoryService.assets.concat(images);
      console.log('assets after', that.directoryService.assets);
      that.directoryService.sortFiles(that.sortState, that.currentDirectoryToString(), that.editor.user.org_logged_as);
    };

    console.log('begin editor', this.editor);
  }
  // Get assets to populate library, video &  images separate //
  setup () {
    console.log('setting up', this.editor.in_library);
    // clear any left over results //
    this.directoryService.assets = [];
    this.currentDirectory = ['' + this.editor.user.organization.id];
    const token = this.cookieManager.get(this.editor.authKey);

    this.mediaLibrarySerive.createClient(this.imageHost, token, 'image');

    this.mediaLibrarySerive.graphGetImages(this.editor.in_library).subscribe( (results) => {
      console.log(results, this.editor.in_library);
      if (results && results['data'] && results['data']['get'] && results['data']['get'][0]['resources']) {
        const images = this.convertFiles(results['data']['get'][0]['resources']);
        console.log('images', images);
        this.directoryService.assets = this.directoryService.assets.concat(images);
        this.directoryService.sortFiles(this.sortState, this.currentDirectoryToString(), this.editor.user.org_logged_as);
      }
      this.isLoading = false;
    });
    // this.mediaLibrarySerive.graphGetVideos(this.videoHost, token).subscribe( (results) => {
    //   const videos = this.convertFiles(results['data']['videos'][0]['resources']);
    //   this.directoryService.assets = this.directoryService.assets.concat(videos);
    //   this.directoryService.sortFiles(this.sortState, this.currentDirectoryToString(), this.editor.user.org_logged_as);
    // });
  }

  // Toggle shown images and show which folder is selected //
  onFolderClick(folder: AssetFile): void {
    this.currentDirectory.push(folder.name);
    this.directoryService.sortFiles(this.sortState, this.currentDirectoryToString(), this.editor.user.org_logged_as);
    this.currentDepth++;
  }

  // When clicking on a directctory slash or the back button //
  onBackPath(depth: number): void {
    // Don't register if last path clicked or at root //
    if (depth !== this.currentDirectory.length && this.currentDirectory.length > 1) {
      // slice end of dirrectory arry if clicked on back button (passed no number)
      depth = depth ? depth : -1;
      this.currentDirectory = this.currentDirectory.slice(0, depth);
      this.currentDepth = (depth < 0) ? this.currentDepth - 1 : depth - 1;

      this.directoryService.sortFiles(this.sortState, this.currentDirectoryToString(), this.editor.user.org_logged_as);
    }
  }

  // add show/hide css class if image in current directory //
  toggleImages (file: any): boolean {
    if (this.term.length > 0) {
      const path = file.type === 'folder' ? file.path : file.name;
      return (this.findFiles(path, this.term));
    }


    if (!file.path) {
      console.log('no ptah', file);
    }

    return (this.directoryService.findMatch(this.currentDirectoryToString(), file));
  }

  getDirPath(): string {
    return this.currentDirectoryToString().replace(/\//g, ' > ');
  }

  currentDirectoryToString(currentDirectory?: string[]): string {
    if (currentDirectory) {
      return currentDirectory.join('/');
    }
    return this.currentDirectory.join('/');
  }

  // find files that match search term //
  findFiles (url: string, term: string): boolean {
    return (url.split('/').pop().indexOf(term) !== -1);
  }

  // Set the search term on keystroke //
  setTerm (term: string): void {
    this.term = term;
  }

  hasTerm () {
    return this.term.length > 0;
  }

  // Get count for all images that are visible //
  getFoundImageCount (): number {
    return this.getVisibleDom('.asset-image');
  }

  // Get count for all images that are visible //
  getFoundFolderCount (): number {
    return this.getVisibleDom('.asset-folder');
  }

  getVisibleDom (selector: string): number {
    let count = 0;
    const assetImages = document.querySelectorAll(selector);
    for (let a = 0; a !== assetImages.length; a++) {
        if (this.isHidden(<Element> assetImages[a])) {
          count++;
        }
    }

    return count;
  }

  isHidden (el: Element): boolean {
    const style = window.getComputedStyle(el);
    return ((style.display === 'none') || (style.visibility === 'hidden'));
  }

  // Toggle for "no results" when no folders or files are visible //
  hasResults (): boolean {
    return (this.getFoundImageCount() > 0 || this.getFoundFolderCount() > 0);
  }

  convertFiles (assets: Array<Resource | AssetFile>): Array<File> {
    const files: any = [];
    for (let a = 0; a !== assets.length; a++) {
      files.push(this.convertToFile(assets[a]));
    }

    return files;
  }

  convertToFile (resource: Resource | AssetFile): AssetFile {
    // console.log('resource', resource);
    const name = resource.name ? resource.name : this.directoryService.getURLFilename(resource);
    const type = resource.type ? resource.type : this.getFileTypeFromName(name);
    const directory = this.directoryService.getURLDirectory(resource);

    return {
      name: name,
      path: this.directoryService.getResourceURL(resource),
      directory: resource.directory,
      type: type,
      pending: false
    };
  }

  isFolderType (file: any): boolean {
    if (file.type) {
      if (file.type.toLowerCase() === 'folder') {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getFileType (file: any): string {
    if (file.type) {
      return file.type;
    } else {
      return this.getFileTypeFromName(file.name);
    }
  }

  getFileTypeFromName (filename: string) {
    const extention: string = filename.split('.').pop();
    if (!extention) {
      return 'generic';
    }

    if (this.isVideoType(extention)) {
      return 'video';
    }
    if (this.isImageType(extention)) {
      return 'image';
    }
  }

  isVideoType (extention: string): boolean {
    extention = extention.indexOf('.') !== -1 ? extention.split('.').pop() : extention;
    const types = ['mp4', 'avi', 'mov'];
    return types.includes(extention);
  }

  isImageType (extention: string): boolean {
    extention = extention.indexOf('.') !== -1 ? extention.split('.').pop() : extention;
    const types: Array<string> = ['jpg', 'png', 'gif'];
    return types.includes(extention);
  }

  sort () {
    this.sortState = this.directoryService.getSortType(this.sortState);
    this.directoryService.sortFiles(this.sortState, this.currentDirectoryToString(), this.editor.user.org_logged_as);
  }

  deleteAsset (file: AssetFile) {
    console.log('delete', file);
    const is_deleting = this.directoryService.deleteAsset(file, this.sortState, this.currentDirectoryToString(),
        this.editor.user.org_logged_as);
    if (is_deleting) {
      this.selectedFile = null;
      if (this.drawer) {
        this.drawer.close();
      }
    }
  }

  hasFiles (): boolean {
    return this.directoryService.files.length === 0;
  }

  dragstart(event: any, file: AssetFile): boolean {
    if (file.pending) {
      return false;
    }
    console.log('drag start', file);
    event.dataTransfer.setData('fileDragged', JSON.stringify(file));

    return true;
  }

  drop(event: any, file: AssetFile): void {
    event.preventDefault();
    const fileDragged = JSON.parse(event.dataTransfer.getData('fileDragged'));
    const fileDroppdOn = file;
    console.log('dragged', fileDragged, 'dropped', fileDroppdOn);

    const filePath = fileDragged.path;
    const folderPath = fileDroppdOn.path;
    this.directoryService.moveFile(fileDragged, fileDroppdOn.path, this.sortState, this.currentDirectoryToString(),
        this.editor.user.org_logged_as);
  }

  allowDrop (event: any): void {
    event.preventDefault();
  }

  showPending (event: any) {
    event.target.onerror = null;
    event.target.src = this.errorImage;
  }

  renameFile (drawerFile: any, newName: string) {
      this.directoryService.renameFile(drawerFile.path, newName, this.sortState,
          this.currentDirectoryToString(), this.editor.user.org_logged_as);
  }

  closeDrawer (event: any) {
    // clear file from memory //
    this.selectedFile = null;
    console.log('event', event);
    // Hide first instance of move-error-flash //
    // this.mediaLibrarySerive.toggleMoveFlash('block');
  }

  singleClick (file: AssetFile): void {
    if (!file.pending) {
      this.preventSimpleClick = false;
      const delay = 200;

      this.timer = setTimeout(() => {
        if (!this.preventSimpleClick) {
          // whatever you want with simple click go here
          console.log('single click');

          this.addFilePullDown(file);

          this.changeDetector.detectChanges();
        }
      }, delay);
    }
  }

  doubleClick (file: AssetFile): void {
    console.log('db click');
    this.preventSimpleClick = true;
    clearTimeout(this.timer);
    // whatever you want with double click go here
    console.log('double click');
    this.isDrawerOpen = false;
    if (file.type === 'folder') {
      this.onFolderClick(file);
    } else {
      this.editor.addAsset(file.path);
      this.changeDetector.detectChanges();
    }
  }

  addFile (result: any) {
    console.log(result);
  }

  getRootDir (): string {
    return this.currentDirectory.shift();
  }

  moveFile (data: any) {
    this.directoryService.moveFile(data.file, data.newPath, this.sortState, this.currentDirectoryToString(),
        this.editor.user.org_logged_as, true);
  }

  addFilePullDown (file: AssetFile) {
    // Add pulldown to body //
    this._pulldown.appendComponentToBody(FilepulldownComponent, this._pulldown.ids.FILE, {file: file,
      directories: this.directoryService.getAllDirectories(file)});
    // Subscribe to optput event emitters //
    this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.move.subscribe( (data) => {
      this.moveFile(data);
      this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.closePulldown();
    });
    this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.rename.subscribe( (newFile) => {
      this.renameFile(newFile, newFile.name);
      this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.closePulldown();
    });
    this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.open.subscribe( (selectedFile) => {
      this.doubleClick(selectedFile);
      this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.closePulldown();
    });
    this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.delete.subscribe( (data) => {
      console.log('delete emitted', data);
      this.deleteAsset(file);
      this._pulldown.pulldowns[this._pulldown.ids.FILE].instance.closePulldown();
    });
  }
}
