import { AngularFireAuth } from '@angular/fire/auth';
import { Organization } from './organization';

export interface User {
  first_name: string;
  last_name: string;
  phone_number: string;
  uid: string;
  email: string;
  refreshToken: string;
  firebase: AngularFireAuth;
  username: string;
  organization: Organization;
  org_logged_as: number;
}