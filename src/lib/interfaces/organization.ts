export interface Organization {
  name: string;
  id: number;
  role: string;
  role_id: number;
}