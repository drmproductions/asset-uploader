import { QueryRef } from 'apollo-angular';
import { DocumentNode } from 'graphql';

export interface GraphHost {
  name: string,
  link: string,
  query: DocumentNode,
  fetch?: QueryRef<object>
}