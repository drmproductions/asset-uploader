export interface Resource {
  url: string;
  s3_path: string;
  old_path: string;
  directory: string;
  type: string;
  name: string;
}