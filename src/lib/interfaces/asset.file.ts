export interface AssetFile {
  name: string;
  path: string;
  type: string;
  directory: string;
  pending: boolean;
}
