import {GraphHost} from './graph.host';

export interface GraphHosts {
  image: GraphHost;
  video: GraphHost
}