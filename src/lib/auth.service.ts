import {Injectable} from '@angular/core';
import { CookieService } from 'ngx-cookie';

import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {HttpService} from './http.service';

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable()
export class AuthService extends HttpService {

  constructor(public _cookieService: CookieService, public http: HttpClient, public dialog: MatDialog) {
    super(http, _cookieService, dialog);
    console.log('auth const');
  }
}
